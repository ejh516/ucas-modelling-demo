#!/usr/bin/env python
#UCAS molecular simulation, Summer 2016
#Launcher
#Written by Robert Forrest & Ryan Cocking

import sys 

sys.path.insert(0, sys.path[0]+'/source')
import scen_gui as gui

gui.begin()
