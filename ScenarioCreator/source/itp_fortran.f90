!f2py -c -m itp_fortran itp_fortran.f90 --opt=-O2

module phys

  implicit none
  private
  public :: potential_force, interaction, thermostat

  integer, parameter :: dp=selected_real_kind(15,300)
  real(kind=dp), parameter :: half = 0.5_dp
  real(kind=dp), parameter :: cut_off = 2.5_dp
  real(kind=dp), parameter :: cut_off_2 = cut_off**2

contains

  subroutine potential_force(mag_r_ij_2, potential, mag_force)
    !Simple LJ contribution

    implicit none
    real(kind=dp), intent(in)                        :: mag_r_ij_2        !Square of the magnitude of separation
    real(kind=dp), intent(out)                       :: potential
    real(kind=dp), intent(out)                       :: mag_force
    real(kind=dp)                                    :: inv_mag_r_squared
    real(kind=dp)                                    :: attractive, repulsive

    inv_mag_r_squared = 1.0_dp/(mag_r_ij_2)

    attractive = inv_mag_r_squared**3
    repulsive = attractive**2

    potential = 4.0_dp*(repulsive - attractive)

    mag_force = -24.0_dp * (attractive-(2.0_dp * repulsive)) * inv_mag_r_squared

  end subroutine potential_force

  subroutine interaction(pos,Natm,Ndim,cutoff_2,force,potential)
    !Calculate all interactions

    implicit none
    integer, intent(in)                                :: Ndim              !Number of dimensions 
    integer, intent(in)                                :: Natm              !Number of atoms
    real(kind=dp), dimension(Natm,Ndim), intent(in)    :: pos               !Positions array
    real(kind=dp), intent(in)                          :: cutoff_2          !Square of cut-off
    real(kind=dp), dimension(Ndim)                     :: r_i, r_j
    real(kind=dp), dimension(Ndim)                     :: r_ij              !Distance between atoms
    real(kind=dp)                                      :: mag_r_ij_2        !Square of the magnitude of separation

    real(kind=dp)                                      :: de
    real(kind=dp)                                      :: dfij

    real(kind=dp), dimension(Natm,Ndim), intent(out)   :: force
    real(kind=dp), intent(out)                         :: potential

    integer :: i, j

    force = 0.0_dp
    potential = 0.0_dp

    !Loop j > i
    do i = 1, Natm
       r_i = pos(i,:)

       do j = i+1, Natm

          r_j = pos(j,:)
          r_ij = r_i-r_j
          mag_r_ij_2 = dot_product(r_ij,r_ij)

          !If within cut off
          if(mag_r_ij_2 < cutoff_2)then

             call potential_force(mag_r_ij_2, de, dfij)
             !Update values
             potential = potential + de
             force(i,:) = force(i,:) + r_ij(:) * dfij
             force(j,:) = force(j,:) - r_ij(:) * dfij

          end if

       end do
    end do

  end subroutine interaction


  subroutine thermostat(force, vel, mu, thermostat_factor, Natm, Ndim)
    implicit none

    integer, intent(in) :: Natm, Ndim
    real(kind=dp), dimension(Natm,Ndim), intent(out) :: force
    real(kind=dp), dimension(Natm,Ndim), intent(in) :: vel
    real(kind=dp), intent(in) :: mu, thermostat_factor
    integer :: i, j

    do i = 1, Natm
       do j = 1, Ndim
          force(i,j) = - mu*vel(i,j) + thermostat_factor*normal_random()
       end do
    end do

  contains
    function uniform_random()
      implicit none
      integer, parameter :: lint = selected_int_kind(20)
      real(kind=dp) :: uniform_random
      integer(kind=lint), parameter :: a=2_lint**32, B=1664525_lint, M=1013904223_lint
      integer(kind=lint), save :: x = 125321_lint

      x = mod(a*x+B,M)
      uniform_random = real(x,dp)/real(M,dp)

    end function uniform_random

    function normal_random()
      implicit none

      real(kind=dp) :: normal_random
      real(kind=dp) :: v1, v2, n, m
      real(kind=dp), save :: second_rand
      logical, save       :: rand_done

      if (rand_done) then
         normal_random = second_rand
         rand_done = .false.
      else
         do
            v1 = 2.0_dp*uniform_random() - 1.0_dp
            v2 = 2.0_dp*uniform_random() - 1.0_dp
            n = v1**2 + v2**2
            if (n < 1.0_dp) exit
         end do
         m = sqrt(-2.0_dp*log(n)/n)
         normal_random = v1*m
         second_rand   = v2*m
         rand_done = .true.
      end if

    end function normal_random


  end subroutine thermostat

  ! subroutine md_step(pos, vel, dt, Natm, Ndim, pe, ke, thermostat_status)
  !   implicit none
  !   real(kind=dp), dimension(Natm,Ndim), intent(inout) :: pos, vel
  !   real(kind=dp), dimension(Natm,Ndim) :: force
  !   real(kind=dp), intent(out) :: pe, ke
  !   real(kind=dp), intent(in)  :: dt
  !   logical, intent(in) :: thermostat_status

  !   call vv_1(pos,vel,dt)

  !   call interaction(pos,Natm,Ndim,cutoff_2,force,potential)

  !   if (thermostat_status) call thermostat(vel)

  !   call vv_2(pos,vel,dt)

  ! end subroutine md_step


  ! subroutine vv_1(pos, vel)
  !   implicit none 
  !   !do first velocity Verlet step to get new values
  !   do iion=1,mdl%cell%num_ions
  !      do i = 1,3
  !         md_vp(i,iion) = md_v0(i,iion) + (md_f0(i,iion) * half_dt)
  !         md_rp(i,iion) = md_r0(i,iion) + (md_vp(i,iion)*dt)
  !      end do
  !   end do

  ! end subroutine vv_1

  ! subroutine vv_2(pos, vel)
  !   implicit none 
  !   !do first velocity Verlet step to get new values
  !   do iion=1,mdl%cell%num_ions
  !      v_fac = hstep/md_mass(iion)
  !      do i = 1,3
  !         md_vp(i,iion) = md_v0(i,iion) + (md_f0(i,iion) * v_fac) - (hstep*mdl%md_chi*md_v0(i,iion))
  !      end do
  !   end do

  ! end subroutine vv_1


end module phys
