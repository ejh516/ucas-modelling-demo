#!/usr/bin/env python
#UCAS molecular simulation, Summer 2016
#GUI Routines
#Written by Robert Forrest & Ryan Cocking

from PyQt4 import QtCore, QtGui, uic
import sys
import _thread
import scen_main as main
import scen_params as params
import os
import time
import atexit

#This function begins the thread for the visual window.
def main_thread():      
    main.run()

#----------------------------#
#            GUI             #
#----------------------------#

#Routines for creating the startup window
startup_file = uic.loadUiType(params.basedir+"/source/gui/startup.ui")[0]  
class Startup(QtGui.QMainWindow, startup_file):
    def __init__(self, parent=None):        
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self) 
        self.center()
        self.logo.setPixmap(QtGui.QPixmap(params.basedir+"/images/logo.svg"))
        self.controls = None
        
        self.beginButton.clicked.connect(self.begin)
        
        #Search for atom data files, and add to the list of atom types
        for file in os.listdir(params.basedir+"/source/scenarios"):
            if file.endswith(".txt"):
                file = file[:-4]
                file = file[:1].upper() + file[1:]
                self.ScenEntry.addItem(file)
        
    def begin(self):
        global vis
        params.scenario = str.lower(str(self.ScenEntry.currentText()))
        params.filebase = params.basedir+'/source/scenarios/'+params.scenario
        import scen_visuals as vis
        
        #Start the visual thread.
        _thread.start_new_thread(main_thread, ())
        #Show the control panel window
        self.controls = Gui(None)
        self.controls.show()
        self.params = Params(None)
        self.params.show()
        self.close()
        
    #Moves window to centre of screen
    def center(self):
        frameGm = self.frameGeometry()
        frameGm.moveCenter(QtGui.QApplication.desktop().screenGeometry(QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())).center())
        self.move(frameGm.topLeft())
        
     #When this window closes
    def closeEvent(self, event):
        if(self.controls == None):
            os._exit(1)


params_file = uic.loadUiType(params.basedir+"/source/gui/params.ui")[0]  
class Params(QtGui.QMainWindow, params_file):
    def __init__(self, parent=None):        
        time.sleep(0.5)
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self) 
        self.updateData()
        self.DumpButt.clicked.connect(lambda:self.output_params(params.filebase+".txt"))

        self.PosEntry.textChanged.connect(self.updateFiles)
        self.VelEntry.textChanged.connect(self.updateFiles)

    def updateFiles(self):
        params.AtomFile = self.PosEntry.text()
        params.VelFile  = self.VelEntry.text()
        
    def updateData(self):
        self.DimEntry.setCurrentIndex(params.Ndim-2)
        if (params.AtomFile == ''):
            self.PosEntry.setText(params.scenario+".xyz")
        else:
            self.PosEntry.setText(params.AtomFile)
        self.SizeEntry.setValue(params.size)
        if (params.VelFile == ''):
            self.VelEntry.setText(params.scenario+".vxyz")
        else:
            self.VelEntry.setText(params.VelFile)
        self.CutEntry.setValue(params.cut_off)
        self.InitTEntry.setValue(params.target_T)
        self.MuEntry.setValue(params.mu)
        self.NavEntry.setValue(params.nav)
        self.SkipEntry.setValue(params.visual_skip)
        self.SpeColEntry.setValue(params.speed_colour)
        self.ThermEntry.setCurrentIndex(int(params.thermostat_status))
        self.dtEntry.setValue(params.dt)
        self.DefEntry.setValue(params.defect)
        self.NEntry.setValue(params.N)
        self.SafeEntry.setValue(params.safety_factor)
        self.SpaceEntry.setValue(params.lattice_constant)
        self.DragEntry.setValue(params.drag_strength)
        self.LinkEntry.setValue(params.bond_strength)
        self.WallEntry.setValue(params.wall_strength)
        #Sync
        self.updateFiles()

        
    def output_params(self,filename):
        out=open(filename,'w')
        if (os.path.isfile(params.basedir+"/source/scenarios/"+self.PosEntry.text())):
            out.write("posXYZ "+self.PosEntry.text()+"\n")
        else:
            out.write("N "+str(self.NEntry.value())+"\n")
            out.write("spacing "+str(self.SpaceEntry.value())+"\n")
            out.write("defect "+str(self.DefEntry.value())+"\n")
            
        if (os.path.isfile(params.basedir+"/source/scenarios/"+self.VelEntry.text())):
            out.write("velXYZ "+self.VelEntry.text()+"\n")
        out.write("Ndim "+str(self.DimEntry.currentIndex()+2)+"\n")
        out.write("dt "+str(self.dtEntry.value())+"\n")
        out.write("mu "+str(self.MuEntry.value())+"\n")
        out.write("size "+str(self.SizeEntry.value())+"\n")
        out.write("therm "+str(self.ThermEntry.currentIndex())+"\n")
        out.write("nav "+str(self.NavEntry.value())+"\n")
        out.write("skip "+str(self.SkipEntry.value())+"\n")
        out.write("speedc "+str(self.SpeColEntry.value())+"\n")
        out.write("init_T "+str(self.InitTEntry.value())+"\n")
        out.write("bond_str "+str(self.LinkEntry.value())+"\n")
        out.write("wall_str "+str(self.WallEntry.value())+"\n")
        out.write("drag_str "+str(self.DragEntry.value())+"\n")
        out.write("rcut "+str(self.CutEntry.value())+"\n")
        out.write("safe "+str(self.SafeEntry.value())+"\n")
        out.write("fixed "+",".join(params.fixed)+"\n")
        if (bool(self.BondsEntry.currentIndex())):
            out.write("bonds True\n")
        else:
            out.write("bonds "+",".join(map(str,sum(params.bonds,[])))+"\n")
        out.close()
        main.restart = True
        
    #When this window closes
    def closeEvent(self, event):
        os._exit(1)


#Routines for the control panel window
controls_file = uic.loadUiType(params.basedir+"/source/gui/scenario.ui")[0]  
class Gui(QtGui.QMainWindow, controls_file):
    def __init__(self, parent=None):
        global vis
        
        #Sleep so main thread has time to initialise values
        time.sleep(0.5)
        
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self) 
        self.logo.setPixmap(QtGui.QPixmap(params.basedir+"/images/UoYLogo.png"))
        
        #Setup button icons
        self.playButton.setIcon(QtGui.QIcon(params.basedir+"/images/play.png"))
        self.pauseButton.setIcon(QtGui.QIcon(params.basedir+"/images/pause.png"))
        self.onestepButton.setIcon(QtGui.QIcon(params.basedir+"/images/onestep.png"))
        
        #Time button event handlers
        self.playButton.clicked.connect(self.play_simulation) 
        self.pauseButton.clicked.connect(self.pause_simulation) 
        self.onestepButton.clicked.connect(self.onestep_simulation) 

        self.MinButton.clicked.connect(self.changeTherm)
        self.DumpButt.clicked.connect(main.dumpScenario) 
        self.QuenchButton.clicked.connect(main.quench)
        
        self.mouseButtons.buttonClicked.connect(self.update_tool)
        i = 0
        for button in self.mouseButtons.buttons():
            i +=1
            self.mouseButtons.setId(button,i)
        
        #Initial values                
        self.skipSlider.setValue(params.visual_skip)
        self.skipLabel_2.setText(str(params.visual_skip))
        
        self.dtSlider.setValue(params.dt*1000.0)
        self.dtLabel_2.setText(str(params.dt))
        
        #Update slider values
        self.dtSlider.valueChanged.connect(self.change_dt)        
        self.skipSlider.valueChanged.connect(self.change_skip)
       
        #Update values of various labels
        QtCore.QTimer.singleShot(300, self.updateCurrentTimeLabel)
        QtCore.QTimer.singleShot(300, self.updateMeasuredTemperatureLabel)
        QtCore.QTimer.singleShot(300, self.updateAverageTLabel)
        QtCore.QTimer.singleShot(300, self.updatePressureLabel)
        QtCore.QTimer.singleShot(300, self.updateFPSLabel)

                
        self.statusBar().showMessage('Running')

    def changeTherm(self):
        if (params.thermostat_status):
            params.thermostat_status = False
            self.MinButton.setText('Turn\nMinimiser\nOn')
        else:
            params.thermostat_status = True
            self.MinButton.setText('Turn\nMinimiser\nOff')
        
    def update_tool(self):
        params.selection = self.mouseButtons.checkedId()

    #When this window closes
    def closeEvent(self, event):

        vis.destroy_objects()                
        vis.contuing = False        
        
        main.end = True
        main.another_frame = False                

        startup = Startup(None)
        startup.show()
        
    #Cause sim to continue 
    def play_simulation(self): 
        main.paused = False
        main.another_frame = True
        self.statusBar().showMessage('Running')
    
    #Cause sim to pause
    def pause_simulation(self): 
        main.paused = True
        self.statusBar().showMessage('Paused')
        
    #Cause sim to increment by a short period of time and stop
    def onestep_simulation(self): 
        main.paused = True
        main.onestep = True
        self.statusBar().showMessage('Incremented by one step')
        
    #Start sim from beginning.
    def restart_simulation(self): 
        main.restart = True
        main.paused = False
        main.onestep = False
        
        #Sleep so that main has enough time to reinitialise
        time.sleep(0.1)
        
        self.statusBar().showMessage('Restarted')
        
        self.dtSlider.setValue(params.dt*1000.0)
        self.dtLabel_2.setText(str(params.dt))
        
        self.skipSlider.setValue(params.visual_skip)
        self.skipLabel_2.setText(str(params.visual_skip))
    
    def change_dt(self):
        params.dt = self.dtSlider.value()/1000.0
        params.inv_dt = 1.0/params.dt
        params.half_dt  = 0.5 * params.dt
        params.update_thermostat()
        self.dtLabel_2.setText(str(round(params.dt,3)))
        
    def updateDtSlider(self):
        self.dtSlider.setValue(params.dt*1000.0)
        self.dtLabel_2.setText(str(params.dt))
        QtCore.QTimer.singleShot(300, self.updateDtSlider)
        
    def change_skip(self):
        params.visual_skip = self.skipSlider.value()
        self.skipLabel_2.setText(str(params.visual_skip))
        
    #Update values of various labels
    def updateCurrentTimeLabel(self):
        self.currentTimeLabel.setText(str(round((main.t*0.02419), 3))+" ps")
        QtCore.QTimer.singleShot(300, self.updateCurrentTimeLabel)
        
    def updatePressureLabel(self):
        self.measuredPressureLabel.setText(str(round(main.pressure,4))+" Pa")
        QtCore.QTimer.singleShot(300, self.updatePressureLabel)
        
    def updateMeasuredTemperatureLabel(self):
        self.measuredTValueLabel.setText(str(round(main.current_T/(0.0084), 2))+" K")
        QtCore.QTimer.singleShot(300, self.updateMeasuredTemperatureLabel)
    
    def updateFPSLabel(self):
        self.fpsMeasuredLabel.setText(str(round(vis.fps,2)))
        QtCore.QTimer.singleShot(300, self.updateFPSLabel)
    
    def updateAverageTLabel(self):
        self.averageTValueLabel.setText(str(round(main.T_average/(0.0084),2))+" K")
        QtCore.QTimer.singleShot(300, self.updateAverageTLabel)
        
           
def begin():
    #Initialise the GUI thread
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_X11InitThreads)
    app = QtGui.QApplication(sys.argv)

    #Show the startup window
    startup = Startup(None)
    startup.show()
    
    app.exec_()


