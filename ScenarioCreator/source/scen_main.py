#!/usr/bin/env python
#UCAS molecular simulation, Summer 2016
#Written by Ryan Cocking & Robert Forrest
import numpy as npy
from sys import stdout, path
from time import sleep
import os
import math
import time
import _thread
from queue import Queue

import scen_visuals as vis
import scen_physics as phys
import scen_params  as params


from cProfile import Profile
from pstats import Stats
from functools import reduce
prof = Profile()
params.basedir = path[0][0:-7] # Find base directory (remove (hopefully) /source)

def abort(ErrMsg):
    print("Error :", ErrMsg)
    os._exit(1)
    

def run():
    
    #Declare global variables which can be accessed by the GUI
    global paused, onestep, restart, end, fresh_run, continuing, another_frame
    global current_T, T_average, pressure, t
    global picked_atom_index
    global pos, vel, force, q
    global displacement
    global dragging

    #This is the first time the sim is running
    fresh_run           = True

    end                 = False
    
    while end == False:
        #Should the sim stop?
        #----------------------------------------------------------------------------------------------------#
        #                                           MAIN PROGRAM                                             #
        #----------------------------------------------------------------------------------------------------#


        #----------------------------#
        #           UNITS            #
        #----------------------------#

         # Get scenario and parameters
        filename = params.filebase+'.txt'
        old = os.path.isfile(filename)
        if (old):
            params.read_params(filename)

        #Force no minimisation
        params.thermostat_status = False
        params.target_T = 0.
        params.mu = 1.0
        params.drag_strength = 10.
        
        t                       = 0.0                                   # Current time

        # GUI controls
        paused                  = True
        onestep                 = False
        restart                 = False
        end                     = False
        fps                     = 0                                     # Frames per second

        T_running_total         = [0.0 for i in range(params.nav)]            # Running average for temperature
        T_average               = 0.0                                   # Average temperature over several timesteps

        # Energy
        KE                      = 0.0                                   # Kinetic Energy of current step
        PE                      = 0.0                                   # Potential Energy of current step
        KE_tot                  = 0.0                                   # Total kinetic energy of the system
        PE_tot                  = 0.0                                   # Total potential energy of the system

        picked_atom_index       = -1                                    # Information from vis about atom being dragged
        displacement            = npy.zeros(2)
        dragging                = False


        #----------------------------#
        #         INITIALISE         #
        #----------------------------#
        #Spawn atoms, get initial positions and velocities
        if old:
            if (params.AtomFile != ''):
                # Read file
                params.N, pos = loadXYZ(params.basedir+'/source/scenarios/'+params.AtomFile)
                params.updateN()
            else:
                # Either take initial pos and vel from scenario file or generate them.
                if ('pos' in params.raw_params):
                    try: 
                        pos                 = npy.asarray(eval(params.raw_params['pos']))
                    except KeyError:
                        abort('No atoms specified in '+scenario)
                    if (pos.shape != (params.N,params.Ndim)):
                        abort('Atom number or dimensions different in pos')
                else:
                    for box in params.spawnbox:
                        pos = phys.spawn(params.lattice_constant, box, params.defect, params.N)
    
            
            # Define velocities by: VelFile, vel, v, init_T
            if params.VelFile != '':
                try_N,vel = loadXYZ(params.basedir+'/source/scenarios/'+params.VelFile)
                if try_N != params.N:
                    abort('Number of velocities not equal to number of atoms')
            elif 'vel' in params.raw_params:
                vel = npy.asarray(eval(params.raw_params['vel']))
            elif 'v' in params.raw_params:
                vel = npy.asarray(eval(params.raw_params['v']))
            else:
                vel = phys.initialise_temp()
            if (vel.shape != (params.N,params.Ndim)):
                abort('Atom number or dimensions different in vel')
        
        else:
            pos = vel = []    

        if(params.bonds is True):
            params.bonds = phys.create_bonds(pos)

        # Calculate initial forces between atoms
        force, PE, pressure     = phys.compute_forces(pos, vel,picked_atom_index, displacement, dragging)

        #Calculate the kinetic energies of all the atoms
        KE = 0.5*reduce(lambda ke, v: ke + npy.dot(v,v), vel, 0)

        #Calculate total KE and PE
        KE_tot                  = KE
        PE_tot                  = PE - params.potential_correction

        #Calculate the total energy of the system
        E_tot                   = KE_tot + PE_tot

        #Calculate the temperature of the system
        current_T               = 0.

        #Create the visuals, if they haven't been already created
        if fresh_run == True:
            # VPython initially calls the window VPython
            if(vis.scene.title == "VPython"):
                vis.build_visuals()
            else:
                vis.destroy_objects()
                vis.update_scene()
                
            vis.build_atoms(pos)
            q = Queue()
            if(params.Ndim == 2):
                q2 = Queue()
                _thread.start_new_thread(vis.update_visuals, (q,q2,))
            else:
                _thread.start_new_thread(vis.update_visuals, (q,))
            fresh_run   = False

        #----------------------------#
        #             RUN            #
        #----------------------------#

        k               = 0
        average_counter = 1
        another_frame   = True
        while another_frame:

            # Update dt based on the current temperature, preventing crashes
            if (not paused):
                paused = safety_check(current_T, paused)
                # params.dt, paused = update_dt(current_T, dt, paused)
                
            #Sleep the thread while paused
            while paused == True:
                # Break out of loop, only to come back to be paused again, thus incrementing by small amount of time
                if onestep == True:
                    onestep = False
                    break
                sleep(0.05)

            # Break out of simulation loop, go back to initialisation
            if restart == True:
                paused = False
                onestep = False
                break            

            if (params.N == 0):
                print("No atoms defined")
                paused = True
                continue

            # Initialise energy totals for this step
            KE_tot              = 0
            PE_tot              = 0

            # Perform several physics calculations per visual step
            for i in range(params.visual_skip):

                # Calculate positions and velocities
                pos, vel           = phys.vv_1(pos, vel, force)

                # Calculate forces and potential energies
                force, PE, pressure     = phys.compute_forces(pos, vel, picked_atom_index, displacement, dragging)

                # Calculate new velocities and kinetic energies
                vel, KE                 = phys.vv_2(vel, force)
                
                # Sum total energies
                KE_tot                  += KE
                PE_tot                  += PE

                t                       += params.dt

            # Get the correct energy totals
            KE_tot              = KE_tot / params.visual_skip
            PE_tot              = PE_tot / params.visual_skip
            
            #Update potential energy with the energy correction
            PE_tot              = PE_tot - params.potential_correction

            # Calculate total energy
            E_tot               = PE_tot + KE_tot

            # Calculate the total temperature of the system
            current_T           = phys.calculate_temperature(KE)

            # Calculate average temperature over recent timesteps
            T_running_total[k%params.nav] = current_T
            T_average = (round(sum(T_running_total)/params.nav,2))
            k += 1

            # Update the visuals with new data
            q.put(pos)
            q.put(vel)
            q.join()

            #Get dragging data from vis thread
            if(params.Ndim == 2):
                dragging = q2.get()
                q2.task_done()
                
                if(dragging):
                    picked_atom_index       = q2.get()
                    q2.task_done()
                    displacement            = q2.get()
                    q2.task_done()


def update_dt(T,paused):

    if (T > 1000):
        paused = True
        print("Simulation too hot, pausing")
    else:
        if (T > 1/(params.safety_factor*params.dt**2)):
            dt = abs(npy.max(npy.sqrt(1/(params.safety_factor*T)) - 0.001, 0.001))
    return dt, paused

def safety_check(T, paused):
    if (T > params.safety_factor):
        paused = True
        print("Simulation too hot, pausing")
    else:
        if (T > 1/(params.safety_factor*params.dt**2)):
            paused = True
            print("dt too large? system going unstable, recommend:", round(abs(npy.max(npy.sqrt(1./(params.safety_factor*T)) - 0.001, 0.001)),3))
    return paused

def loadXYZ(filename):
    import re
    global N
    
    out_data = []
    with open(filename) as f_in:
        for line in params.nonblank_lines(f_in):
            words = line.split()
            if (re.match('[0-9]+\s*$',line)):
                N = int(words[0])
            elif (re.match('\s*[A-Z][a-z]{,2}(\s+[0-9.e+-]+){3}$',line)):
                out_data.append(npy.asarray(words[1:params.Ndim+1],dtype=float))
            else:
                continue
    if (len(out_data) != N):
        abort('Number of atoms does not match N in '+filename)
    out_data = npy.asfortranarray(out_data)
    return N,out_data

def dumpScenario():
    global pos, vel

    f = open(params.basedir+"/source/scenarios/"+params.AtomFile,'w')
    f.write(str(params.N)+"\n")
    f.write("Atomic Postitions for "+params.scenario+"\n")
    for atom in pos:
        f.write("Ar "+" ".join(map(str,atom[0:params.Ndim]))+" ".join([" 0.0" for i in range(3-params.Ndim)])+"\n")
    f.close()
    
    f = open(params.basedir+"/source/scenarios/"+params.VelFile,'w')
    f.write(str(params.N)+"\n")
    f.write("Atomic Postitions for "+params.scenario+"\n")
    for atom in vel:
        f.write("Ar "+" ".join(map(str,atom[0:params.Ndim]))+" ".join([" 0.0" for i in range(3-params.Ndim)])+"\n")
    f.close()
    

def addAtom(position):
    global pos, vel, force
    params.updateN(1)
    pos = npy.append(pos,position).reshape(params.N,2)
    vel = npy.append(vel,npy.zeros(params.Ndim)).reshape(params.N,2)
    force = npy.append(force,npy.zeros(params.Ndim)).reshape(params.N,2)
    vis.redraw_atoms(pos)
    
def deleteAtom(index):
    global pos, vel, force
    params.updateN(-1)
    pos = npy.delete(pos,index, 0)
    vel = npy.delete(vel,index, 0)
    force = npy.delete(force, index, 0)
    vis.redraw_atoms(pos)
    
def fixAtom(index):
    global vel, force
    if index in params.fixed:
        del params.fixed[params.fixed.index(index)]
    else:
        params.fixed.append(index)
        vel[index] = npy.zeros(2)
        force[index] = npy.zeros(2)

def bondAtom(bond):
    if bond in params.bonds:
        del params.bonds[params.bonds.index(bond)]
    else:
        params.bonds.append(bond)

def quench():
    global q, pos,vel

    vel = npy.zeros((params.N,params.Ndim))
    q = Queue()
    q.put(pos)
    q.put(vel)
