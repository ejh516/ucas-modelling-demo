
from math import sqrt
from sys import stdout
# Set Defaults
raw_params           = {}
basedir              = ''
scenario             = ''
filebase             = basedir+'/source/scenarios/'
AtomFile             = ''
VelFile              = ''
Ndim                 = 2
dt                   = 0.02
half_dt              = 0.5*dt
inv_dt               = 1./dt
mu                   = 0.5
defect               = 0.
size                 = 5.
thermostat_status    = False
nav                  = 100
visual_skip          = 1
speed_colour         = 50.
N                    = 0
ndof                 = 0
target_T             = 0.
thermostat_factor    = 0.
box_width            = size
lattice_constant     = 1.05
bond_strength        = 100.
wall_strength        = 50.
drag_strength        = 0.5
cut_off              = 2.5
cut_off_2            = 6.25
potential_correction = (0.5*(N-1.)*N)*(4. * ((cut_off**-12) - (cut_off**-6)))
safety_factor        = 4000.
selection            = 1
fixed                = []
bonds                = []
spawnbox             = [[[3.,box_width-3.] for i in range(Ndim)]]

def updateN(Ninc=0):
    global N, ndof, potential_correction, box_width
    N += Ninc
    ndof = Ndim*N - len(fixed)
    potential_correction = (0.5*(N-1.)*N)*(4. * ((cut_off**-12) - (cut_off**-6)))
    box_width     = size + (N/10.)

def dumpParams():
    print("raw_params",raw_params)
    print("basedir",basedir)
    print("AtomFile",AtomFile)
    print("VelFile",VelFile)
    print("Ndim",Ndim)
    print("dt",dt)
    print("half_dt",half_dt)
    print("inv_dt",inv_dt)
    print("mu",mu)
    print("defect",defect)
    print("size",size)
    print("thermostat_status",thermostat_status)
    print("nav",nav)
    print("visual_skip",visual_skip)
    print("speed_colour",speed_colour)
    print("N",N)
    print("ndof",ndof)
    print("target_T",target_T)
    print("thermostat_factor",thermostat_factor)
    print("box_width",box_width)
    print("lattice_constant",lattice_constant)
    print("bond_strength",bond_strength)
    print("wall_strength",wall_strength)
    print("drag_strength",drag_strength)
    print("cut_off",cut_off)
    print("cut_off_",cut_off_2)
    print("potential_correction",potential_correction)
    print("safety_factor",safety_factor)
    print("bonds", bonds)
    print("fixed", fixed)
    
def update_thermostat():
    global thermostat_factor
    thermostat_factor = sqrt(2.0*mu*inv_dt*target_T)

def nonblank_lines(f):
    for l in f:
        line = l.rstrip()
        if line:
            yield line


def read_params(filename):
    global raw_params,AtomFile,VelFile,Ndim,dt,half_dt,inv_dt,mu,defect,size,thermostat_status,nav,visual_skip,speed_colour,N,ndof,target_T,thermostat_factor,box_width,lattice_constant,bond_strength,wall_strength,drag_strength,cut_off,cut_off_2,potential_correction,safety_factor, fixed, bonds, spawnbox
    raw_params = {}
    with open(filename) as f:
        for line in nonblank_lines(f):
            if (len(line.split()) > 1):
                (key, val) = line.split()
                raw_params[key] = val
    AtomFile             = raw_params.get('posXYZ','')
    VelFile              = raw_params.get('velXYZ','')
    Ndim                 = int(raw_params.get('ndim',2))
    dt                   = float(raw_params.get('dt',0.02))          # Timestep
    half_dt              = 0.5*dt
    inv_dt               = 1./dt
    mu                   = float(raw_params.get('mu',0.5))          # Coefficient of friction
    defect               = float(raw_params.get('defect',0.))
    size                 = float(raw_params.get('size',5.))          # Default size
    thermostat_status    = (raw_params.get('therm','False')) in ['True','true','1','On','on']       # Default no thermostat
    nav                  = int(raw_params.get('av_len',100))
    visual_skip          = int(raw_params.get('skip',1))             # Number of physics calculations per visual step
    speed_colour         = float(raw_params.get('speedc',50.))
    N                    = int(raw_params.get('N',0))
    target_T             = float(raw_params.get('init_T',0.))
    thermostat_factor    = sqrt(2.0*mu*inv_dt*target_T)
    wall_strength        = float(raw_params.get('wall_str',50.))
    bond_strength        = float(raw_params.get('bond_str',100.))
    drag_strength        = float(raw_params.get('drag_str',0.5))
    cut_off              = float(raw_params.get('rcut',2.5))
    cut_off_2            = cut_off**2
    safety_factor        = float(raw_params.get('safe',4000.))
    lattice_constant     = float(raw_params.get('spacing',1.05))
    fixed                = raw_params.get('fixed','')
    if (fixed != ''):
        fixed = list(map(int,fixed.split(',')))
    else:
        fixed = []
    updateN()
    # Create bonds between all atoms, or take bond information from scenario file
    bonds                = raw_params.get('bonds',[])
    if (bonds):
        if (bonds.lower() in ['t','1','true','all','auto']):
            bonds        = True
        else:
            bonds        = bonds.split(',').reshape(-1,2)
    spawnbox             = raw_params.get('spawn','')
    if (spawnbox != ''):
        spawnbox = map(float,spawnbox.split(':').split(',')).reshape(-1,Ndim)
    else:
        spawnbox         = [[[3.,box_width-3.] for i in range(Ndim)]]
