#!/usr/bin/env python
#UCAS molecular simulation, Summer 2016
#2D Physics Routines
#Written by Ryan Cocking & Robert Forrest

import numpy as npy
import sys
from sys import stdout
from time import sleep
import random as rand
import scen_params as params

import itp_fortran as fort
from functools import reduce

#----------------------------#
#      PHYSICS ROUTINES      #
#----------------------------#
# Set velocities for current temperature
def initialise_temp(temperature=params.target_T):
    if temperature < 1e-8:
        return npy.zeros((params.N,params.Ndim))
    # Factor for Maxwell Boltzmann distribution
    factor = npy.sqrt(temperature)
    # Create random data
    vel = factor*npy.random.normal(size=(params.N,params.Ndim))

    KE_temp = reduce(lambda ke, v: ke + npy.dot(v,v), vel, 0)    
    KE_temp /= params.ndof
    scale = npy.sqrt(temperature/KE_temp)
    vel *= scale
    return vel

#Create lattice on which to spawn atoms
def make_lattice(spacing, width):
    # Make square lattice
    if (params.Ndim == 2):
        points = npy.mgrid[width[0][0]:width[0][1]:spacing, width[1][0]:width[1][1]:spacing].reshape(2,-1).T
    elif (params.Ndim == 3):
        points = npy.mgrid[width[0][0]:width[0][1]:spacing, width[1][0]:width[1][1]:spacing, width[2][0]:width[2][1]:spacing].reshape(3,-1).T
        
    return points
    
def spawn(spacing = None, width = None, defect = None, num = None):
    # Generate a lattice
    if not spacing:
        spacing = params.lattice_constant
    if not width:
        width = [[3.,params.box_width-3.] for i in range(params.Ndim)]
    if not defect:
        defect = params.defect
    grid                = make_lattice(spacing, width)
    if not num:
        num = len(grid)
    # Initial positions and velocities
    pos_init            = npy.zeros((params.N,params.Ndim), dtype=float, order='F')
    i                   = 0

    for pos in grid:
        if rand.random() > defect: #Create point defects
            pos_init[i] = pos
            i+=1
        if (i == num):
            break

    return pos_init

def wall_collisions(pos, f_array, PE):

    total_Wall_Force    = 0

    # Possibly faster still?
    for i in npy.column_stack(npy.where(npy.logical_or(pos < 0.5, pos > params.box_width-0.5))):
        tmp = 0.5 - pos[i[0],i[1]] if pos[i[0],i[1]] < 0.5 else params.box_width-0.5-pos[i[0],i[1]]
        wall_force = params.wall_strength*tmp
        f_array[i[0],i[1]] += wall_force
        total_Wall_Force += abs(wall_force)
        PE += 0.5*params.wall_strength*tmp**2

    # Hard Wall
    # for i in npy.column_stack(npy.where (pos > params.box_width)):
    #     pos[i[0],i[1]] = 2*params.box_width - pos[i[0],i[1]]
    #     vel[i[0],i[1]] = -vel[i[0],i[1]]
    # for i in npy.column_stack(npy.where (pos < 0)):
    #     pos[i[0],i[1]] = -pos[i[0],i[1]]
    #     vel[i[0],i[1]] = -vel[i[0],i[1]]
    # return pos, vel

    return [f_array, PE, total_Wall_Force]

# Calculate bonds
def bond_interactions(pos, f_array, PE):
   
    #Loop through bond list
    for bond in params.bonds:
        #Distance between bonded atoms
        r_ij                    = pos[bond[0]]-pos[bond[1]]
        mag_r_ij_2              = npy.dot(r_ij,r_ij)
        mag_r_ij                = npy.sqrt(mag_r_ij_2)
        
        #Calculate bonding force
        bonding_force           = params.bond_strength*r_ij
        f_array[bond[0]]        -= bonding_force
        f_array[bond[1]]        += bonding_force

        #Calculate PE of bond
        PE                      += 0.5 * params.bond_strength * mag_r_ij_2

    return f_array, PE

#Compute interatomic forces for N atoms
def compute_forces(pos, vel, picked_atom_index, displacement, dragging):

    if (params.N == 0):
        return [[], 0., 0.]
    zero = npy.zeros(2)
    
    # Array of forces
    f_array                             = npy.zeros((params.N,params.Ndim), dtype=float, order='F')
    # Total potential energy
    PE                                  = 0

    #Compute interactions between atoms
    f_array, PE = fort.phys.interaction(pos, params.cut_off_2)
    #Compute wall interactions
    
    f_array, PE, total_Wall_Force       = wall_collisions(pos, f_array, PE)
    pressure                            = total_Wall_Force / (params.box_width**2)

    # Calculate bond forces
    if (len(params.bonds)):
        f_array, PE                     = bond_interactions(pos, f_array, PE)

    # Is an atom being dragged with the mouse?
    if(dragging == True):
        dragging_force                  = displacement * params.drag_strength
        f_array[picked_atom_index]      += dragging_force

    # Is the thermostat active?
    if (params.thermostat_status == True):
        f_array += fort.phys.thermostat(vel, params.mu, params.thermostat_factor)
    
    # Remove forces on fixed atoms
    for fix in params.fixed:
        f_array[fix] = zero
        vel[fix] = zero
        
    return [f_array, PE, pressure]

#Calculate future atomic positions and velocities with velocity-verlet algorithm
#Two components: (1) called prior to force calculation, (2) called after
def vv_1(pos, vel, force):

    vel_temp = vel + params.half_dt*force
    pos_new  = pos + vel_temp*params.dt

    return [pos_new, vel_temp]

#Second part of velocity verlet algorithm
def vv_2(vel_temp, force):

    #Calculate new velocities
    vel_new = vel_temp + params.half_dt*force
    KE_new  = 0.5*reduce(lambda ke, v: ke + npy.dot(v,v), vel_new, 0)

    return [vel_new, KE_new]

# Create bonds between all atoms close enough
def create_bonds(pos):
    bonds = []
    
    for i in range(params.N):
        for j in range(i+1,params.N):
            
            r_ij = pos[i]-pos[j]
            mag_r_ij_2 = npy.dot(r_ij,r_ij)
            
            if (mag_r_ij_2 < 1.69):
                bonds.append([i,j])

    return bonds

def calculate_temperature(KE):
    return 2.*KE/params.ndof
