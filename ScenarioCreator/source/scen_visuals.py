#!/usr/bin/env python
#UCAS molecular simulation, Summer 2016
#3D Visual Python Routines
#Written by Ryan Cocking & Robert Forrest

from visual import *
import scen_main as main
import scen_params as params
import numpy as npy
import time

#----------------------------#
#          DRAWING           #
#----------------------------#

#Draw particle container
def build_box(x,y,z=(0.,0.)):
    # Front face
    bounding_box = [curve(pos=[(x[0],y[0],z[0]), (x[1],y[0],z[0]),
                (x[1],y[1],z[0]), (x[0],y[1],z[0]), 
                (x[0],y[0],z[0])])]
    if (params.Ndim == 3):
        # Back face
        bounding_box.append(curve(pos=[(x[0],y[0],z[1]), (x[1],y[0],z[1]),
                   (x[1],y[1],z[1]), (x[0],y[1],z[1]), 
                   (x[0],y[0],z[1])]))
        # Connecting lines
        bounding_box.append(curve(pos=[(x[0],y[0],z[0]), (x[0],y[0],z[1])]))
        bounding_box.append(curve(pos=[(x[1],y[0],z[0]), (x[1],y[0],z[1])]))
        bounding_box.append(curve(pos=[(x[0],y[1],z[0]), (x[0],y[1],z[1])]))
        bounding_box.append(curve(pos=[(x[1],y[1],z[0]), (x[1],y[1],z[1])]))
    
    return bounding_box

#Initialise visuals
def build_visuals():
    global scene, fps, bounding_box

    #Create the scene
    scene = display(title='Illustrating Theoretical Physics', x=360, y=30, width=700, height=700, center = [params.box_width*0.5 for i in range(params.Ndim)],range=(1.1*(params.box_width/2.0)))
    scene.lights        = []
    scene.ambient       = color.white
    fps                 = 0

    bounding_box = build_box(*([0,params.box_width] for i in range(params.Ndim)))
    if (params.Ndim == 2):
        scene.userspin      = False
        scene.userzoom      = False     
    return None

def build_atoms(pos):
    global atoms
    # Create array of atoms
    atoms       = npy.asarray([sphere(pos=vector(atom), radius=0.5, color=color.blue) for atom in pos])
    
def update_scene():
    global bounding_box
    global scene
    
    #Create the bounding box
    bounding_box = build_box(*((0,params.box_width) for i in range(params.Ndim)))
    #Center and range the scene
    scene.center = [params.box_width*0.5 for i in range(params.Ndim)]
    scene.range         = 0.55*params.box_width

# Update positions and colours of atoms
def update_visuals(q,q2=None):
    global atoms
    global scene
    global continuing
    global bond_line
    global fps

    # Initialise bond lines
    if len(params.bonds) > 0:
        bond_line = [curve(pos=[vector(atoms[bond[0]].pos), vector(atoms[bond[1]].pos)], radius=0.06) for bond in params.bonds]
    else:
        bond_line = []
    
    # Initial values for fps counter
    frame_times         = []
    start_t             = time.time()
    
    continuing = True
    while continuing:
        #Limit framerate so that sim doesnt run too fast
        rate(100)
        
        # Get values from main thread
        pos     = q.get()
        v       = q.get()
        if (q2):
            #Handle moving individual atoms with the mouse
            picked_atom_index       = -1
            dragging_force          = npy.zeros(2)
            dragging                = False
            drag_line               = None
            already_updated         = False

            # If there is a mouse click
            if scene.mouse.events:
                m1 = scene.mouse.getevent()
                if m1.click:
                    if   params.selection == 1:
                        new_pos = npy.asarray(scene.mouse.project(normal=(0,0,1)))[0:params.Ndim]
                        if (all([sum([i**2 for i in x]) > params.lattice_constant*0.75 for x in pos - new_pos])):
                            main.addAtom(new_pos)
                    elif params.selection == 2:
                        if m1.pick:
                            picked_atom_index = npy.where(atoms==m1.pick)
                            if (len(picked_atom_index[0]) > 0):
                                picked_atom_index = picked_atom_index[0][0]
                                main.deleteAtom(picked_atom_index)
                    elif params.selection == 3:
                        pass
                    elif params.selection == 4:
                        pass
                    elif params.selection == 5:
                        if m1.pick:
                            picked_atom_index = npy.where(atoms==m1.pick)
                            if (len(picked_atom_index[0]) > 0):
                                picked_atom_index = picked_atom_index[0][0]
                                main.fixAtom(picked_atom_index)
                elif m1.drag:
                    if   params.selection == 1:
                        init_pos = npy.asarray(scene.mouse.project(normal=(0,0,1)))[0:params.Ndim]
                    elif params.selection == 2:
                        init_pos = npy.asarray(scene.mouse.project(normal=(0,0,1)))[0:params.Ndim]
                    elif params.selection == 3:
                        if m1.pick:
                            # Find atom clicked on
                            picked_atom_index       = npy.where(atoms==m1.pick)
                            picked_atom_index       = picked_atom_index[0][0]
                            dragging                = True
                    elif params.selection == 4:
                        init_pos = npy.where(atoms==m1.pick)
                        if (len(init_pos[0]) > 0):
                            init_pos = init_pos[0][0]
                            
                    elif params.selection == 5:
                        pass
                elif m1.drop:
                    if params.selection == 1:
                        final_pos = npy.asarray(scene.mouse.project(normal=(0,0,1)))[0:params.Ndim]
                    elif params.selection == 2:
                        final_pos = npy.asarray(scene.mouse.project(normal=(0,0,1)))[0:params.Ndim]
                    elif params.selection == 3:
                        final_pos = npy.asarray(scene.mouse.project(normal=(0,0,1)))[0:params.Ndim]
                    elif params.selection == 4:
                        final_pos = npy.where(atoms==m1.pick)
                        if (len(final_pos[0]) > 0):
                            final_pos = final_pos[0][0]
                            main.bondAtom([init_pos,final_pos])
                    elif params.selection == 5:
                        final_pos = npy.asarray(scene.mouse.project(normal=(0,0,1)))[0:params.Ndim]
                        
            #While an atom is being dragged
            while not main.paused and dragging:
                rate(100)
                
                collected_from_queue        = False
                # If needed, collect data from main thread
                if(pos==None):
                    collected_from_queue    = True
                    pos                     = q.get()
                    v                       = q.get()
                else:
                    q.task_done()
                    q.task_done()
                
                # project mouse pos onto xy plane
                new_pos     = scene.mouse.project(normal=(0,0,1))
    
                # Draw dragging line
                if(drag_line):
                    drag_line.visible = False
                    del drag_line
                drag_line = curve(pos=[vector(pos[picked_atom_index]), new_pos], radius=0.05)
                
                # Calculate displacement between atom and mouse pos
                displacement        = npy.asarray(new_pos)[0:2] - pos[picked_atom_index]
                # Place dragging data into queue, to be collected by main thread
                q2.put(dragging)
                q2.put(picked_atom_index)
                q2.put(displacement)
                
                # Check if dragging has stopped
                if scene.mouse.events:
                    m1 = scene.mouse.getevent()
                    if m1.drop:
                        main.picked_atom_index      = -1
                        main.displacement           = npy.zeros(2)
                        main.dragging               = False
                        dragging                    = False
                        drag_line.visible           = False
                        del drag_line
                
                #Update positions and colours
                already_updated     = True
                update_positions_colors(pos, v, atoms, bond_line)
                
                pos                 = None
                T                   = None
                
                if(collected_from_queue==True):
                    q.task_done()
                    q.task_done()
            
            # Make sure that visuals havent already been updated in dragging loop
            if(already_updated==False):
                update_positions_colors(pos, v, atoms, bond_line)
                q2.put(dragging)
                
                q.task_done()
                q.task_done()
        else:
            update_positions_colors(pos, v, atoms, bond_line)
            q.task_done()
            q.task_done()
        
        # Calculate frames per second
        end_t           = time.time()
        time_taken      = end_t - start_t
        start_t         = end_t
        frame_times.append(time_taken)
        frame_times     = frame_times[-20:]
        fps             = len(frame_times) / sum(frame_times)
        
def update_positions_colors(pos, v, atoms, bond_line):
    #Update positions and colours
    for i in range(min(len(pos),len(atoms))):
        atoms[i].pos    = pos[i]
        
        #Produce a value proportional to atom speed, used for colouring
        speed           = npy.sqrt(npy.dot(v[i],v[i]))
        normalised_v    = (speed*params.N)/(params.speed_colour)
            
        #Colour the atoms based on their speed
        atoms[i].color  = (normalised_v,0,1.-normalised_v)
        
    # Fixed atoms are gray
    for fix in params.fixed:
        atoms[fix].color = color.gray(0.5)
        
    # Create bond lines
    for i in range(len(params.bonds)):
        if i > len(bond_line)-1:
            bond_line.append(curve(pos=[vector(pos[params.bonds[i][0]]), vector(pos[params.bonds[i][1]])], radius=0.06))
            bond_line[-1].visible = True
        elif(bond_line[i]!=0):
            bond_line[i].visible = False
        else:
            bond_line[i] = curve(pos=[vector(pos[params.bonds[i][0]]), vector(pos[params.bonds[i][1]])], radius=0.06)
 
    
# Remove all visual objects
def destroy_objects():
    global atoms
    global bond_line
    global bounding_box

    for bond in bond_line:
        bond.visible = False
    for atom in atoms:
        atom.visible = False
    for line in bounding_box:
        line.visible = False


def redraw_atoms(pos):
    global atoms
    global bond_line
    for bond in bond_line:
        bond.visible = False
    for atom in atoms:
        atom.visible = False
    atoms       = npy.asarray([sphere(pos=vector(atom), radius=0.5, color=color.blue) for atom in pos])
    
