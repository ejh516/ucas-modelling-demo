#!/bin/bash

cwd=`pwd`

rm -fr ucas_python bin/modelling_demo source/*.so

python3 -m venv ucas_python             && \
  source ucas_python/bin/activate       && \
  pip install PyQt5 numpy pygame meson ninja

cd source && make && cd $cwd || exit 1

mkdir -p bin

cat <<EOF > ./bin/modelling_demo
#!/bin/bash
cd \$(dirname \$(dirname \$(realpath \$0)))
source ucas_python/bin/activate
python ./source/modelling_demo.py
EOF

chmod +x bin/modelling_demo
