!f2py -c -m itp_fortran itp_fortran.f90 --opt=-O2
!If this fails: f2py3 -c -m itp_fortran itp_fortran.f90 --opt=-O2
!Use this for flags f2py  -c -m itp_fortran itp_fortran.f90 --opt=-O2 --f90flags=-C 


module phys

  implicit none
  private
  public :: potential_force, interaction, thermostat

  integer, parameter :: dp=8
!  real*8, parameter :: half = 0.5_dp
!  real*8, parameter :: cut_off = 2.5_dp
!  real*8, parameter :: cut_off_2 = cut_off**2
  real*8 :: half = 0.5_dp
  real*8 :: cut_off = 2.5_dp
  real*8 :: cut_off_2 = 2.5_dp**2
contains

  subroutine potential_force(mag_r_ij_2, potential, mag_force)
    !Simple LJ contribution

    implicit none
    real*8, intent(in)                        :: mag_r_ij_2        !Square of the magnitude of separation
    real*8, intent(out)                       :: potential
    real*8, intent(out)                       :: mag_force
    real*8                                    :: inv_mag_r_squared
    real*8                                    :: attractive, repulsive
    real*8                                    :: epsilon

    ! Lennard-Jones parameters
    epsilon = 10.0_dp
    
    inv_mag_r_squared = 1.0_dp/(mag_r_ij_2)

    attractive = inv_mag_r_squared**3
    repulsive = attractive**2

    potential = 4.0_dp*epsilon*(repulsive - attractive)

    mag_force = -24.0_dp * epsilon*(attractive-(2.0_dp * repulsive)) * inv_mag_r_squared

  end subroutine potential_force

  subroutine interaction(pos,Natm,Ndim,cutoff_2,force,potential, Temp,P)
    !Calculate all interactions

    implicit none
    integer, intent(in)                                :: Ndim              !Number of dimensions 
    integer, intent(in)                                :: Natm              !Number of atoms
    integer, intent(in)                                :: P
    real*8, dimension(Natm,Ndim,0:P-1), intent(in)  :: pos               !Positions array
    real*8, intent(in)                          :: cutoff_2          !Square of cut-off
    real*8, dimension(Ndim)                     :: r_i, r_j
    real*8, dimension(Ndim)                     :: r_ij              !Distance between atoms
    real*8                                      :: mag_r_ij_2        !Square of the magnitude of separation
    real*8, intent(in)                          :: Temp              !current temperature
    real*8                                      :: de
    real*8                                      :: dfij
    real*8                                      ::m                  !mass
    real*8                                      ::beta               !beta factor, = to 1/KbT
    real*8                                      ::omega              !angular frequency = sqrt(P)/beta
    real*8, dimension(Natm,Ndim,0:P-1), intent(out)   :: force
    real*8, dimension(0:P-1), intent(out)             :: potential

    integer :: i, j, k, S1, S2

    force = 0.0_dp
    potential = 0.0_dp
    m =1.0_dp
    beta=1.0_dp/Temp
    omega=sqrt(real(P,kind=dp))/beta               
    
    do k = 0, P-1
      do i = 1, Natm
         r_i = pos(i,:,k)

         do j = i+1, Natm
         
            r_j = pos(j,:,k)
            r_ij = r_i-r_j
            mag_r_ij_2 = dot_product(r_ij,r_ij)

            !If within cut off
            if(mag_r_ij_2 < cutoff_2)then

               call potential_force(mag_r_ij_2, de, dfij)
               !Update values
               potential(k) = potential(k) + de
               force(i,:,k) = force(i,:,k) + r_ij(:) * dfij
               force(j,:,k) = force(j,:,k) - r_ij(:) * dfij
               

            end if
            
        end do
      end do
      
      !for quantum simulation, update total force by using the spring constant
      if (P .gt. 1) then 
      S1 = mod(k+1,P)
      S2 = mod(P+k-1,P)
    
      !here there are two options, using 1/P of the initial force (commented below) or the total initial force
      !force(:,:,k) = (1.0_dp/real(P,kind=dp))*force(:,:,k) - m*omega**2*(2.0_dp*pos(:,:,k)-pos(:,:,S1)-pos(:,:,S2))
      force(:,:,k) = force(:,:,k) - m*omega**2*(2.0_dp*pos(:,:,k)-pos(:,:,S1)-pos(:,:,S2))
      end if
    end do
    beta=0.0_dp
    omega=0.0_dp
  end subroutine interaction


  subroutine thermostat(force, vel, mu, thermostat_factor, Natm, Ndim,P)
    implicit none

    integer, intent(in) :: Natm, Ndim,P
    real*8, dimension(Natm,Ndim,P), intent(out) :: force
    real*8, dimension(Natm,Ndim,P), intent(in) :: vel
    real*8, intent(in) :: mu, thermostat_factor
    integer :: i, j, k

    do i = 1, Natm
       do j = 1, Ndim
         do k = 1, P
            force(i,j,k) = - mu*vel(i,j,k) + thermostat_factor*normal_random()
         end do
       end do
    end do
  contains
    function uniform_random()
      implicit none
      integer, parameter :: lint = selected_int_kind(20)
      real*8 :: uniform_random
      integer(kind=lint), parameter :: a=2_lint**32, B=1664525_lint, M=1013904223_lint
      integer(kind=lint), save :: x = 125321_lint

      x = mod(a*x+B,M)
      uniform_random = real(x,dp)/real(M,dp)

    end function uniform_random

    function normal_random()
      implicit none

      real*8 :: normal_random
      real*8 :: v1, v2, n, m
      real*8, save :: second_rand
      logical, save       :: rand_done

      if (rand_done) then
         normal_random = second_rand
         rand_done = .false.
      else
         do
            v1 = 2.0_dp*uniform_random() - 1.0_dp
            v2 = 2.0_dp*uniform_random() - 1.0_dp
            n = v1**2 + v2**2
            if (n < 1.0_dp) exit
         end do
         m = sqrt(-2.0_dp*log(n)/n)
         normal_random = v1*m
         second_rand   = v2*m
         rand_done = .true.
      end if

    end function normal_random


  end subroutine thermostat

  ! subroutine md_step(pos, vel, dt, Natm, Ndim, pe, ke, thermostat_status)
  !   implicit none
  !   real*8, dimension(Natm,Ndim), intent(inout) :: pos, vel
  !   real*8, dimension(Natm,Ndim) :: force
  !   real*8, intent(out) :: pe, ke
  !   real*8, intent(in)  :: dt
  !   logical, intent(in) :: thermostat_status

  !   call vv_1(pos,vel,dt)

  !   call interaction(pos,Natm,Ndim,cutoff_2,force,potential)

  !   if (thermostat_status) call thermostat(vel)

  !   call vv_2(pos,vel,dt)

  ! end subroutine md_step


  ! subroutine vv_1(pos, vel)
  !   implicit none 
  !   !do first velocity Verlet step to get new values
  !   do iion=1,mdl%cell%num_ions
  !      do i = 1,3
  !         md_vp(i,iion) = md_v0(i,iion) + (md_f0(i,iion) * half_dt)
  !         md_rp(i,iion) = md_r0(i,iion) + (md_vp(i,iion)*dt)
  !      end do
  !   end do

  ! end subroutine vv_1

  ! subroutine vv_2(pos, vel)
  !   implicit none 
  !   !do first velocity Verlet step to get new values
  !   do iion=1,mdl%cell%num_ions
  !      v_fac = hstep/md_mass(iion)
  !      do i = 1,3
  !         md_vp(i,iion) = md_v0(i,iion) + (md_f0(i,iion) * v_fac) - (hstep*mdl%md_chi*md_v0(i,iion))
  !      end do
  !   end do

  ! end subroutine vv_1


end module phys
