#!/usr/bin/env python
#UCAS molecular simulation, Summer 2016
#GUI Routines
#Written by Robert Forrest & Ryan Cocking

import sys
import _thread as thread
import os
import time
import atexit
from PyQt5 import QtCore, QtGui, QtWidgets, uic

import itp_main as main
import itp_params as params
import itp_visuals as vis

#This function begins the thread for the visual window.
def main_thread(scenario):
    main.run(scenario)

#----------------------------#
#            GUI             #
#----------------------------#

#Routines for creating the startup window
startup_file = uic.loadUiType(params.basedir+"/source/gui/startup.ui")[0]
class Startup(QtWidgets.QMainWindow, startup_file):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.center()
        self.logo.setPixmap(QtGui.QPixmap(params.basedir+"/images/logo.svg"))
        self.controls = None

        self.beginButton.clicked.connect(lambda:self.begin())

        #Search for atom data files, and add to the list of atom types
        for file in os.listdir(params.basedir+"/source/scenarios"):
            if file.endswith(".txt"):
                file = file[:-4]
                file = file[:1].upper() + file[1:]
                self.scenarioList.addItem(file)

    def begin(self):
        scenario = str.lower(str(self.scenarioList.currentText()))

        #Start the visual thread.
        thread.start_new_thread(main_thread, (scenario,))
        #Show the control panel window
        self.controls = Gui(None)
        self.controls.show()
        self.close()

    #Moves window to centre of screen
    def center(self):
        frameGm = self.frameGeometry()
        frameGm.moveCenter(QtWidgets.QApplication.desktop().screenGeometry(QtWidgets.QApplication.desktop().screenNumber(QtWidgets.QApplication.desktop().cursor().pos())).center())
        self.move(frameGm.topLeft())

     #When this window closes
    def closeEvent(self, event):
        if(self.controls == None):
            os._exit(1)


#Routines for the control panel window
controls_file = uic.loadUiType(params.basedir+"/source/gui/controls.ui")[0]
class Gui(QtWidgets.QMainWindow, controls_file):
    def __init__(self, parent=None):
        global vis
        global stage1_done, stage2_done, stage3_done, stage4_done, stage5_done, stage6_done

        #Sleep so main thread has time to initialise values
        time.sleep(0.5)

        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.logo.setPixmap(QtGui.QPixmap(params.basedir+"/images/UoYLogo.png"))

        #Setup button icons
        self.coldButton.setIcon(QtGui.QIcon(params.basedir+"/images/down.png"))
        self.hotButton.setIcon(QtGui.QIcon(params.basedir+"/images/up.png"))
        self.playButton.setIcon(QtGui.QIcon(params.basedir+"/images/play.png"))
        self.pauseButton.setIcon(QtGui.QIcon(params.basedir+"/images/pause.png"))
        self.onestepButton.setIcon(QtGui.QIcon(params.basedir+"/images/onestep.png"))
        self.restartButton.setIcon(QtGui.QIcon(params.basedir+"/images/reset.png"))

        #Thermostat button event handlers
        self.coldButton.clicked.connect(lambda:self.change_temperature(-10))
        self.hotButton.clicked.connect(lambda:self.change_temperature(10))
        self.thermostatStatusButton.clicked.connect(lambda:self.changeThermostatStatus())

        #Time button event handlers
        self.playButton.clicked.connect(lambda:self.play_simulation())
        self.pauseButton.clicked.connect(lambda:self.pause_simulation())
        self.onestepButton.clicked.connect(lambda:self.onestep_simulation())
        self.restartButton.clicked.connect(lambda:self.restart_simulation())

        #Initial values
        self.skipSlider.setValue(params.visual_skip)
        self.skipLabel_2.setText(str(params.visual_skip))

        #self.dtSlider.setValue(params.dt*1000.0)
        self.dtSlider.setValue(round(params.dt*1000))
        self.dtLabel_2.setText(str(params.dt))

        self.checkThermostatStatus()

        #Update slider values
        self.dtSlider.valueChanged.connect(lambda:self.change_dt())
        self.skipSlider.valueChanged.connect(lambda:self.change_skip())

        #Update values of various labels
        QtCore.QTimer.singleShot(300, lambda: self.updateDtSlider())
        QtCore.QTimer.singleShot(300, lambda: self.updateCurrentTimeLabel())
        QtCore.QTimer.singleShot(300, lambda: self.updateMeasuredTemperatureLabel())#				fix here
        QtCore.QTimer.singleShot(300, lambda: self.updateAverageTLabel())
        QtCore.QTimer.singleShot(300, lambda: self.updateTargetTempLabel())
        QtCore.QTimer.singleShot(300, lambda: self.updatePressureLabel())
        QtCore.QTimer.singleShot(300, lambda: self.updateFPSLabel())

        #Run temperature cycle if in display mode
        if(main.scenario_out == "display"):

            self.thermostatStatusButton.setEnabled(False)
            self.thermostatStatusButton.setStyleSheet('background-color: None')
            self.coldButton.setEnabled(False)
            self.hotButton.setEnabled(False)

            start = time.time()
            stage1_done = False
            stage2_done = False
            stage3_done = False
            stage4_done = False
            stage5_done = False
            stage6_done = False
            QtCore.QTimer.singleShot(300, lambda: self.temperature_cycle(start))

        self.statusBar().showMessage('Running')

    #When this window closes
    def closeEvent(self, event):

        vis.destroy_objects()
        vis.contuing = False

        main.end = True
        main.another_frame = False

        startup = Startup(None)
        startup.show()

    #Cause sim to continue
    def play_simulation(self):
        main.paused = False
        self.statusBar().showMessage('Running')

    #Cause sim to pause
    def pause_simulation(self):
        main.paused = True
        self.statusBar().showMessage('Paused')

    #Cause sim to increment by a short period of time and stop
    def onestep_simulation(self):
        main.paused = True
        main.onestep = True
        self.statusBar().showMessage('Incremented by one step')

    #Start sim from beginning.
    def restart_simulation(self):
        main.restart = True
        main.paused = False
        main.onestep = False

        #Sleep so that main has enough time to reinitialise
        time.sleep(0.1)

        self.statusBar().showMessage('Restarted')
        self.checkThermostatStatus()
        self.targetTValueLabel.setText(str(params.target_T))

        #self.dtSlider.setValue(params.dt*1000.0)
        self.dtSlider.setValue(round(params.dt*1000))
        self.dtLabel_2.setText(str(params.dt))

        self.skipSlider.setValue(params.visual_skip)
        self.skipLabel_2.setText(str(params.visual_skip))


    def checkThermostatStatus(self):
        if params.thermostat_status == False:
            self.thermostatStatusButton.setText("Turn On")
            self.thermostatStatusButton.setStyleSheet("background-color: green")
        else:
            self.thermostatStatusButton.setText("Turn Off")
            self.thermostatStatusButton.setStyleSheet("background-color: red")


    #Turn the Thermostat on or off
    def changeThermostatStatus(self):
        params.thermostat_status = not params.thermostat_status
        self.checkThermostatStatus()

    #Change the target temperature of the thermostat
    def change_temperature(self, val):
        max_T = 1000
        modifiers = QtWidgets.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ShiftModifier:
            next_temperature = (params.target_T/0.0084)+10*val
        else:
            next_temperature = (params.target_T/0.0084)+val

        if 0 <= next_temperature <= max_T:
            params.target_T=next_temperature*(0.0084)
            self.targetTValueLabel.setText(str(round(params.target_T/(0.0084),2))+" K")
        elif next_temperature < 0:
            params.target_T=0
            self.targetTValueLabel.setText(str(round(params.target_T/(0.0084),2))+" K")
        elif next_temperature > max_T:
            params.target_T=max_T*(0.0084)
            self.targetTValueLabel.setText(str(round(params.target_T/(0.0084),2))+" K")
        params.update_thermostat()

    def change_dt(self):
        params.dt = self.dtSlider.value()/1000.0
        params.inv_dt = 1.0/params.dt
        params.half_dt  = 0.5 * params.dt
        params.update_thermostat()
        self.dtLabel_2.setText(str(round(params.dt,3)))

    def updateDtSlider(self):
#        self.dtSlider.setValue(params.dt*1000.0)
        self.dtSlider.setValue(round(params.dt*1000))
        self.dtLabel_2.setText(str(params.dt))
        QtCore.QTimer.singleShot(300, lambda: self.updateDtSlider())

    def change_skip(self):
        params.visual_skip = self.skipSlider.value()
        self.skipLabel_2.setText(str(params.visual_skip))

    #Update values of various labels
    def updateCurrentTimeLabel(self):
        self.currentTimeLabel.setText(str(round((main.t*0.02419), 3))+" ps")
        QtCore.QTimer.singleShot(300, lambda: self.updateCurrentTimeLabel())

    def updatePressureLabel(self):
        self.measuredPressureLabel.setText(str(round(main.Fakepressure,4))+" Pa")
        QtCore.QTimer.singleShot(300, lambda: self.updatePressureLabel())

    def updateMeasuredTemperatureLabel(self):
        self.measuredTValueLabel.setText(str(round(main.current_T/(0.0084), 2))+" K")
        QtCore.QTimer.singleShot(300, lambda: self.updateMeasuredTemperatureLabel())

    def updateFPSLabel(self):
        self.fpsMeasuredLabel.setText(str(round(vis.fps,2)))
        QtCore.QTimer.singleShot(300, lambda: self.updateFPSLabel())

    def updateAverageTLabel(self):
        self.averageTValueLabel.setText(str(round(main.T_average/(0.0084),2))+" K")
        QtCore.QTimer.singleShot(300, lambda: self.updateAverageTLabel())

    def updateTargetTempLabel(self):
        self.targetTValueLabel.setText(str(round(params.target_T/(0.0084),2))+" K")
        QtCore.QTimer.singleShot(300, lambda: self.updateTargetTempLabel())

    def temperature_cycle(self, start):
        global stage1_done, stage2_done, stage3_done, stage4_done, stage5_done, stage6_done

        stage_length = 30

        time_difference = time.time() - start
        if stage1_done==False and time_difference > stage_length:
            stage1_done = True
            QtCore.QTimer.singleShot(1000, lambda: self.temperature_cycle_up(10, 100))

        elif stage2_done==False and time_difference > stage_length:
            stage2_done = True
            QtCore.QTimer.singleShot(1000, lambda: self.temperature_cycle_up(10, 200))

        elif stage3_done==False and time_difference > stage_length:
            stage3_done = True
            QtCore.QTimer.singleShot(1000, lambda: self.temperature_cycle_up(10, 300))

        elif stage4_done==False and time_difference > stage_length:
            stage4_done = True
            QtCore.QTimer.singleShot(1000, lambda: self.temperature_cycle_down(10, 200))

        elif stage5_done==False and time_difference > stage_length:
            stage5_done = True
            QtCore.QTimer.singleShot(1000, lambda: self.temperature_cycle_down(10, 100))

        elif stage6_done==False and time_difference > stage_length:
            stage6_done = True
            QtCore.QTimer.singleShot(1000, lambda: self.temperature_cycle_down(10, 0))

        elif stage6_done==True and time_difference > stage_length:
            stage1_done = False
            stage2_done = False
            stage3_done = False
            stage4_done = False
            stage5_done = False
            stage6_done = False
            QtCore.QTimer.singleShot(300, lambda: self.temperature_cycle(start))

        else:
            QtCore.QTimer.singleShot(300, lambda: self.temperature_cycle(start))

    def temperature_cycle_up(self, increment, limit):

        params.target_T += increment*(0.0084)
        params.update_thermostat()
        if params.target_T < limit*(0.0084):
            QtCore.QTimer.singleShot(1000, lambda: self.temperature_cycle_up(increment, limit))
        else:
            QtCore.QTimer.singleShot(300, lambda: self.temperature_cycle(time.time()))

    def temperature_cycle_down(self, increment, limit):

        next_temperature = params.target_T - increment*(0.0084)
        if(next_temperature < 0):
               next_temperature = 0
        params.target_T = next_temperature
        if params.target_T > limit*(0.0084):
            QtCore.QTimer.singleShot(1000, lambda: self.temperature_cycle_down(increment, limit))
        else:
            QtCore.QTimer.singleShot(300, lambda: self.temperature_cycle(time.time()))
        params.update_thermostat()

def begin():
    #Initialise the GUI thread
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_X11InitThreads)
    app = QtWidgets.QApplication(sys.argv)

    #Show the startup window
    startup = Startup(None)
    startup.show()

    app.exec_()


