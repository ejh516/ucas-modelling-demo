#!/usr/bin/env python
#UCAS molecular simulation, Summer 2016
#Written by Ryan Cocking & Robert Forrest
import numpy as npy
from sys import stdout, path
from time import sleep
import os
import math
import time
import _thread as thread
from queue import Queue
from cProfile import Profile
from pstats import Stats
from functools import reduce

import itp_visuals as vis
import itp_physics as phys
import itp_params  as params


prof = Profile()
params.basedir = path[0][0:-7] # Find base directory (remove (hopefully) /source)

def abort(ErrMsg):
    print("Error :", ErrMsg)
    os._exit(1)


def run(scenario):

    #Declare global variables which can be accessed by the GUI
    global paused, onestep, restart, end, fresh_run, continuing, another_frame
    global current_T, T_average, pressure, t, Fakepressure
    global picked_atom_index
    global displacement
    global dragging
    global scenario_out

    scenario_out = scenario


    #This is the first time the sim is running
    fresh_run           = True

    end                 = False

    while end == False:
        #Should the sim stop?
        #----------------------------------------------------------------------------------------------------#
        #                                           MAIN PROGRAM                                             #
        #----------------------------------------------------------------------------------------------------#


        #----------------------------#
        #           UNITS            #
        #----------------------------#

         # Get scenario and parameters
        filename        = params.basedir+'/source/scenarios/'+scenario+'.txt'
        params.read_params(filename)

        t                       = 0.0                                   # Current time

        # GUI controls
        paused                  = True
        onestep                 = False
        restart                 = False
        end                     = False
        fps                     = 0                                     # Frames per second

        T_running_total         = [0.0 for i in range(params.nav)]            # Running average for temperature
        T_average               = 0.0                                   # Average temperature over several timesteps

        # Energy
        KE                      = 0.0                                   # Kinetic Energy of current step
        PE                      = 0.0                                   # Potential Energy of current step
        KE_tot                  = 0.0                                   # Total kinetic energy of the system
        PE_tot                  = 0.0                                   # Total potential energy of the system
        Fakepressure            = 0.0
        picked_atom_index       = -1                                    # Information from vis about atom being dragged
        displacement            = npy.zeros(2)
        dragging                = False


        #----------------------------#
        #         INITIALISE         #
        #----------------------------#
        #Spawn atoms, get initial positions and velocities
        if (params.AtomFile != ''):
            # Read file
            params.N  = loadXYZ(params.basedir+'/source/scenarios/'+params.AtomFile,"N") #"N" argument gets the value of N
            pos = npy.empty((params.N,params.Ndim,params.P))
            pos[:,:,0] = loadXYZ(params.basedir+'/source/scenarios/'+params.AtomFile,"out_data") #"out_data" argument gets all the positon of the atoms
            #Create a box to spawn atoms in
            if params.N >= 452 :
                params.box_width = npy.ceil(params.size + (params.N/10.))
            else :
                params.box_width = npy.ceil(params.size + (452/10.))
            params.ndof = params.Ndim*params.N

            r = 0.2  #half radius of gyration, Just a constant here, should be calculated using formula

            #Assign a position to all quantum state using the radius of gyration
            for i in range(-1,params.P) :
                if i == 1 :
                   pos[:,:,i]  = pos[:,:,0]-[r,0]
                if i == 2 :
                   pos[:,:,i]  = pos[:,:,0]-[0,r]
                if i == 3 :
                   pos[:,:,i]  = pos[:,:,0]+[r,0]
                if i == 4 :
                   pos[:,:,i]  = pos[:,:,0]-[r/math.sqrt(2),r/math.sqrt(2)]
                if i == 5 :
                   pos[:,:,i]  = pos[:,:,0]+[r/math.sqrt(2),-r/math.sqrt(2)]
                if i == 6 :
                   pos[:,:,i]  = pos[:,:,0]+[r/math.sqrt(2),r/math.sqrt(2)]
                if i == 7 :
                   pos[:,:,i]  = pos[:,:,0]+ [-r/math.sqrt(2),r/math.sqrt(2)]
                if i == -1 :
                   pos[:,:,0]  = pos[:,:,0]+[0,r]

        else:
            # Either take initial pos and vel from scenario file or generate them.
            params.box_width               = npy.ceil(params.size + (params.N/10.))
            if ('pos' in params.raw_params):
                try:
                    pos                 = npy.asarray(eval(params.raw_params['pos']))
                except KeyError:
                    abort('No atoms specified in '+scenario)
                if (pos.shape != (params.N,params.Ndim)):
                    abort('Atom number or dimensions different in pos')
            else:
                pos = phys.spawn(params.lattice_constant, params.box_width, params.defect)


        # Atoms which are fixed in place
        fixed = params.raw_params.get('fixed','')
        if (fixed != ''):
            fixed = list(map(int,fixed.split(',')))
            params.ndof  -= params.Ndim*len(fixed)

        # Define velocities by: VelFile, vel, v, init_T
        if params.VelFile != '':
            vel = npy.empty((params.N,params.Ndim,params.P))
            try_N,vel[:,:,0] = loadXYZ(params.basedir+'/source/scenarios/'+params.VelFile,"Vel") # "Vel" argument gets the velocities and N
            for i in range(0,params.P) :
               vel[:,:,i] = vel[:,:,0]

            if try_N != params.N:
                abort('Number of velocities not equal to number of atoms')
        elif 'vel' in params.raw_params:
            vel = npy.asarray(eval(params.raw_params['vel']))
        elif 'v' in params.raw_params:
            vel = npy.asarray(eval(params.raw_params['v']))
        else:
            vel = phys.initialise_temp()
        if (vel.shape != (params.N,params.Ndim,params.P)):
            abort('Atom number or dimensions different in vel')

        # Create bonds between all atoms, or take bond information from scenario file
        bonds                   = eval(params.raw_params.get('bonds','[]'))
        if(bonds is True):
            bonds = phys.create_bonds(pos)
        elif not isinstance(bonds, (list,tuple,npy.ndarray)):
            abort('Bonds not set correctly')

        # Calculate initial forces between atoms
        PE                      = npy.zeros(params.P)
        pressure                = npy.zeros(params.P)
        force                   = npy.empty((params.N,params.Ndim,params.P))
        current_T               = 0.0
        force, PE, pressure     = phys.compute_forces(pos, vel,picked_atom_index, displacement, dragging, bonds, fixed, current_T)

        #Calculate the kinetic energies of all the atoms
        #If there are multiple beads, the Kinetic enery is calculated from the velocity of the centroid
        KE = 0.0
        Vel_centroid = npy.zeros((params.N,params.Ndim))
        for i in range(0,params.P) :
           Vel_centroid =Vel_centroid + vel[:,:,i]
        Vel_centroid = Vel_centroid/params.P
        KE = 0.5*reduce(lambda ke, v: ke + npy.dot(v,v), Vel_centroid, 0)

        #Calculate total KE and PE
        KE_tot                  = KE
        PE_tot                  = PE - params.potential_correction

        #Calculate the total energy of the system
        E_tot                   = KE_tot + PE_tot

        #Calculate the temperature of the system
        current_T               = phys.calculate_temperature(KE)
        #Create the visuals, if they haven't been already created
        if fresh_run == True:

            vis.bonds = bonds
            vis.fixed = fixed

            # Initialise the visuals and start the update loop all in a new thread
            q = Queue()
            if(params.Ndim == 2):
                q2 = Queue()
                thread.start_new_thread(vis.start_visuals, (pos, q,q2,))
            else:
                thread.start_new_thread(vis.start_visuals, (pos, q,))
            fresh_run   = False

        #----------------------------#
        #             RUN            #
        #----------------------------#

        k               = 0
        average_counter = 1
        another_frame   = True
        while another_frame:
            # Update dt based on the current temperature, preventing crashes
            if (not paused):
                paused = safety_check(current_T, paused)
                # params.dt, paused = update_dt(current_T, dt, paused)

            #Sleep the thread while paused
            while paused == True:
                # Break out of loop, only to come back to be paused again, thus incrementing by small amount of time
                if onestep == True:
                    onestep             = False
                    break
                sleep(0.05)

            # Break out of simulation loop, go back to initialisation
            if restart == True:
                paused = False
                onestep = False
                break

            # Initialise energy totals for this step
            KE_tot              = npy.zeros(params.P)
            PE_tot              = npy.zeros(params.P)

            # Perform several physics calculations per visual step
            for i in range(params.visual_skip):

                # Calculate positions and velocities
                pos, vel_temp           = phys.vv_1(pos, vel, force)

                # Calculate forces and potential energies
                force, PE, pressure     = phys.compute_forces(pos, vel_temp, picked_atom_index, displacement, dragging, bonds, fixed, current_T)

                # Calculate new velocities and kinetic energies
                vel, KE                 = phys.vv_2(vel_temp, force)

                # Sum total energies
                KE_tot                  += KE
                PE_tot                  += PE
                #calculate total pressure as the average of the pressure of all quantum state. This is probably not correct
                for j in range(0,params.P):
                   Fakepressure += pressure[j]
                Fakepressure = Fakepressure/params.P
                t                       += params.dt

            # Get the correct energy totals
            KE_tot              = KE_tot / params.visual_skip
            PE_tot              = PE_tot / params.visual_skip

            #Update potential energy with the energy correction
            PE_tot              = PE_tot - params.potential_correction

            # Calculate total energy
            E_tot               = PE_tot + KE_tot

            # Calculate the total temperature of the system


            current_T           = phys.calculate_temperature(KE)

            # Calculate average temperature over recent timesteps
            T_running_total[k%params.nav] = current_T
            T_average = (round(sum(T_running_total)/params.nav,2))
            k += 1

            # Update the visuals with new data
            q.put(pos)
            q.put(vel)
            q.join()

            #Get dragging data from vis thread
           # if(params.Ndim == 2):
            #    dragging = q2.get()
             #   q2.task_done()

              #  if(dragging):
               #     picked_atom_index       = q2.get()
                #    q2.task_done()
                 #   displacement            = q2.get()
                  #  q2.task_done()


def update_dt(T,paused):

    if (T > 10000):
        paused = True
        print("Simulation too hot, pausing")
    else:
        if (T > 1/(params.safety_factor*params.dt**2)):
            dt = abs(npy.max([npy.sqrt(1/(params.safety_factor*T)) - 0.001, 0.001]))
    return dt, paused

def safety_check(T, paused):
    if (T > 10000):
        paused = True
        print("Simulation too hot, pausing")
    else:
        if (T > 1/(params.safety_factor*params.dt**2)):
            paused = True
            print("dt too large? system going unstable, recommend:", round(abs(npy.max([npy.sqrt(1./(params.safety_factor*T)) - 0.001, 0.001])),3))
    return paused

def loadXYZ(filename,case):
    import re
    global N

    out_data = []
    with open(filename) as f_in:
        for line in params.nonblank_lines(f_in):
            words = line.split()
            if (re.match('[0-9]+\s*$',line)):
                N = int(words[0])
            elif (re.match('\s*[A-Z][a-z]{,2}(\s+[0-9.e+-]+){3}$',line)):
                out_data.append(npy.asarray(words[1:params.Ndim+1],dtype=float))
            else:
                continue
    if (len(out_data) != N):
        abort('Number of atoms does not match N in '+filename)
    out_data = npy.asfortranarray(out_data)
    if case == "N" :
       return N
    if case == "out_data" :
        return out_data
    else :
        return N,out_data
