from math import sqrt
from sys import stdout

# Set Defaults
raw_params           = {}
basedir              = ''
AtomFile             = ''
VelFile              = ''
Ndim                 = 2
dt                   = 0.02
half_dt              = 0.5*dt
inv_dt               = 1./dt
mu                   = 0.5
defect               = 0.
size                 = 5.
thermostat_status    = False
nav                  = 100
visual_skip          = 1
speed_colour         = 50.
N                    = 0
ndof                 = 0
target_T             = 0.
thermostat_factor    = 0.
box_width            = 0.
lattice_constant     = 1.05
bond_strength        = 100.
wall_strength        = 50.
drag_strength        = 0.5
cut_off              = 2.5
cut_off_2            = 6.25
potential_correction = (0.5*(N-1.)*N)*(4. * ((cut_off**-12) - (cut_off**-6)))
safety_factor        = 4000.
fixed                = []
bonds                = []
P                    = 1

def updateN(Ninc=0):
    global N, ndof, potential_correction, box_width
    N += Ninc
    ndof = Ndim*N - len(fixed)
    potential_correction = (0.5*(N-1.)*N)*(4. * ((cut_off**-12) - (cut_off**-6)))
    box_width     = size + (N/10.)

def dump_params():
    from os import stdout
    print("raw_params",raw_params,file=stdout)
    print("basedir",basedir,file=stdout)
    print("AtomFile",AtomFile,file=stdout)
    print("VelFile",VelFile,file=stdout)
    print("Ndim",Ndim,file=stdout)
    print("dt",dt,file=stdout)
    print("half_dt",half_dt,file=stdout)
    print("inv_dt",inv_dt,file=stdout)
    print("mu",mu,file=stdout)
    print("defect",defect,file=stdout)
    print("size",size,file=stdout)
    print("thermostat_status",thermostat_status,file=stdout)
    print("nav",nav,file=stdout)
    print("visual_skip",visual_skip,file=stdout)
    print("speed_colour",speed_colour,file=stdout)
    print("N",N,file=stdout)
    print("ndof",ndof,file=stdout)
    print("target_T",target_T,file=stdout)
    print("thermostat_factor",thermostat_factor,file=stdout)
    print("box_width",box_width,file=stdout)
    print("lattice_constant",lattice_constant,file=stdout)
    print("bond_strength",bond_strength,file=stdout)
    print("wall_strength",wall_strength,file=stdout)
    print("drag_strength",drag_strength,file=stdout)
    print("cut_off",cut_off,file=stdout)
    print("cut_off_",cut_off_2,file=stdout)
    print("potential_correction",potential_correction,file=stdout)
    print("safety_factor",safety_factor,file=stdout)

def output_params(filename):
    print("posXYZ",AtomFile, file=filename)
    print("velXYZ",VelFile, file=filename)
    print("Ndim",Ndim, file=filename)
    print("dt",dt, file=filename)
    print("mu",mu, file=filename)
    print("defect",defect, file=filename)
    print("size",size, file=filename)
    print("therm",thermostat_status, file=filename)
    print("nav",nav, file=filename)
    print("skip",visual_skip, file=filename)
    print("speedc",speed_colour, file=filename)
    print("N",N, file=filename)
    print("init_T",target_T, file=filename)
    print("spacing",lattice_constant, file=filename)
    print("bond_str",bond_strength, file=filename)
    print("wall_str",wall_strength, file=filename)
    print("drag_str",drag_strength, file=filename)
    print("rcut",cut_off, file=filename)
    print("safe",safety_factor, file=filename)


def update_thermostat():
    global thermostat_factor
    # The following seems to assume kb=1, since target_T is treated as the thermal energy
    thermostat_factor = sqrt(2.0*mu*inv_dt*target_T*P)               # *P for P beads

def nonblank_lines(f):
    for l in f:
        line = l.rstrip()
        if line:
            yield line


def read_params(filename):
    global raw_params,AtomFile,VelFile,Ndim,dt,half_dt,inv_dt,mu,defect,size,thermostat_status,nav,visual_skip,speed_colour,N,ndof,target_T,thermostat_factor,box_width,lattice_constant,bond_strength,wall_strength,drag_strength,cut_off,cut_off_2,potential_correction,safety_factor,P
    raw_params = {}
    with open(filename) as f:
        for line in nonblank_lines(f):
            if (len(line.split()) > 1):
                (key, val) = line.split()
                raw_params[key] = val
    AtomFile             = raw_params.get('posXYZ','')
    VelFile              = raw_params.get('velXYZ','')
    Ndim                 = int(raw_params.get('ndim',2))
    dt                   = float(raw_params.get('dt',0.02))          # Timestep
    half_dt              = 0.5*dt
    inv_dt               = 1./dt
    mu                   = float(raw_params.get('mu',0.5))          # Coefficient of friction
    defect               = float(raw_params.get('defect',0.))
    size                 = float(raw_params.get('size',5.))          # Default size
    thermostat_status    = (raw_params.get('therm','False')) in ['True','true','1','On','on']       # Default no thermostat
    nav                  = int(raw_params.get('av_len',100))
    visual_skip          = int(raw_params.get('skip',1))             # Number of physics calculations per visual step
    speed_colour          = float(raw_params.get('speedc',50.))
    N                    = int(raw_params.get('N',0))
    target_T             = float(raw_params.get('init_T',0.))
    thermostat_factor    = sqrt(2.0*mu*inv_dt*target_T)
    wall_strength        = float(raw_params.get('wall_str',50.))
    bond_strength        = float(raw_params.get('bond_str',100.))
    drag_strength        = float(raw_params.get('drag_str',0.5))
    cut_off              = float(raw_params.get('rcut',2.5))
    cut_off_2            = cut_off**2
    safety_factor        = float(raw_params.get('safe',4000.))
    lattice_constant     = float(raw_params.get('spacing',1.05))
    fixed                = raw_params.get('fixed','')
    P                    = int(raw_params.get('P',''))          #Number of beads, up to 8

    if (fixed != ''):
        fixed = list(map(int,fixed.split(',')))
    else:
        fixed = []
    updateN()
    # Create bonds between all atoms, or take bond information from scenario file
    bonds                = raw_params.get('bonds',[])
    if (bonds):
        if (bonds.lower() in ['t','1','true','all','auto']):
            bonds        = True
        else:
            bonds        = bonds.split(',').reshape(-1,2)
