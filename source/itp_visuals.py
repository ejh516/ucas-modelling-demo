#!/usr/bin/env python
#UCAS molecular simulation, Summer 2016
#3D Visual Python Routines
#Written by Ryan Cocking & Robert Forrest

import numpy as npy
import time
from random import random
from math import sqrt

import pygame
import pygame.locals as pg

import itp_params as params

scene = None
atoms=[]
fps=0

#----------------------------#
#          DRAWING           #
#----------------------------#

black   = (   0,   0,   0 )
white   = ( 255, 255, 255 )
grey    = ( 127, 127, 127 )
blue    = (   0,   0, 255 )
green   = (   0, 255,   0 )
red     = ( 255,   0,   0 )
cyan    = (   0, 255, 255 )
magenta = ( 255,   0, 255 )
yellow  = ( 255, 255,   0 )
orange  = ( 255, 127,   0 )
colours = (blue, yellow, red, green, cyan, magenta, white, orange)

class PGScene:
    def __init__(self, window_size):
        self.window_size = window_size
        self.title = ""
        self.screen = pygame.display.set_mode((self.window_size, self.window_size), pg.RESIZABLE)
        self.clock = pygame.time.Clock()
        self.scale  = (self.window_size - 10) / params.box_width
        self.atoms = []

        print(f"Mode: {pygame.display.get_driver()}")

class Atom:
    def __init__(self, colour):
        self.size       = 1.0
        self.colour     = colour
        self.beads      = []
        self.centroid   = []
        self.r_gyration = []

    def update_atom(self, beads):
        self.beads = beads
        self.centroid = [ sum(self.beads[0,:])/params.P,
                          sum(self.beads[1,:])/params.P ]

        self.r_gyration = [ sqrt(abs(sum(self.beads[0,:]**2)/params.P - self.centroid[0]**2)),
                            sqrt(abs(sum(self.beads[1,:]**2)/params.P - self.centroid[1]**2)) ]


def draw_bounding_box(screen):
    pygame.draw.line(screen, white, [5,  5],                                    [5, scene.window_size-5],                   5)
    pygame.draw.line(screen, white, [5, scene.window_size-5],                   [scene.window_size-5, scene.window_size-5], 5)
    pygame.draw.line(screen, white, [scene.window_size-5, scene.window_size-5], [scene.window_size-5, 5],                   5)
    pygame.draw.line(screen, white, [scene.window_size-5, 5],                   [5, 5],                                     5)

def draw_scene(scene):
    scene.screen.fill(black)
    for atom in scene.atoms:
        pygame.draw.ellipse(scene.screen, atom.colour, [ scene.window_size - (atom.centroid[0]*scene.scale),
                                                         scene.window_size - (atom.centroid[1]*scene.scale),
                                                         (atom.size + atom.r_gyration[0]) * scene.scale,
                                                         (atom.size + atom.r_gyration[1]) * scene.scale ],
                                                       0 )

    # Limit to 30 frames per second
    draw_bounding_box(scene.screen)

    # flip is faster, but can cause GL issues when threading
    #    pygame.display.flip()
    pygame.display.flip()
    scene.clock.tick(30)

#Initialise visuals
def build_visuals():
    global scene
    pygame.init()
    scene = PGScene(window_size = 950)
    draw_scene(scene)
    return None

def build_atoms(pos):
    global scene
    for iatom in range(0, params.N):
        scene.atoms.append(Atom(colour=T_colour(0)))
        scene.atoms[-1].update_atom(pos[iatom,:,:])

    draw_scene(scene)

    return None

# Build visuals and start the update loop
def start_visuals(pos, q, q2=None):
    build_visuals()
    build_atoms(pos)
    update_visuals(q, q2=None)

# Update positions and colours of atoms
def update_visuals(q, q2=None):
    global continuing, scene, fps, fixed

    # VPython initially calls the window VPython
    frame_times         = []
    start_t             = time.time()

    done = False
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            elif event.type == pygame.VIDEORESIZE:
                scene.window_size = min(event.dict['size'])
                scene.screen = pygame.display.set_mode((scene.window_size, scene.window_size),pg.RESIZABLE)
                scene.scale = (scene.window_size - 10) / params.box_width



        # Get values from main thread
        pos     = q.get()
        v       = q.get()
        update_positions_colours(pos, v, scene.atoms, fixed)
        draw_scene(scene)
        q.task_done()
        q.task_done()

        end_t           = time.time()
        time_taken      = end_t - start_t
        start_t         = end_t
        frame_times.append(time_taken)
        frame_times     = frame_times[-20:]
        fps             = len(frame_times) / sum(frame_times)

    pygame.quit()
    return None

def update_positions_colours(pos, v, atoms, fixed):
    #Update positions and colours
    for iatom in range(0, params.N):
        atoms[iatom].update_atom(pos[iatom,:,:])

        velocity = v[:,:,0]
        speed           = npy.sqrt(npy.dot(velocity[iatom],velocity[iatom]))
        normalised_v    = min(1, (speed*params.N)/(params.speed_colour))

        #Colour the atoms based on their speed
        atoms[iatom].colour  = T_colour(normalised_v)


    return None

def T_colour(T):
    if (T < 0.1000):
        c = (0xff, 0x38, 0x00, 128)
    elif (T < 0.1200):
        c = (0xff, 0x53, 0x00, 128)
    elif (T < 0.1400):
        c = (0xff, 0x65, 0x00, 128)
    elif (T < 0.1600):
        c = (0xff, 0x73, 0x00, 128)
    elif (T < 0.1800):
        c = (0xff, 0x7e, 0x00, 128)
    elif (T < 0.2000):
        c = (0xff, 0x89, 0x12, 128)
    elif (T < 0.2200):
        c = (0xff, 0x93, 0x2c, 128)
    elif (T < 0.2400):
        c = (0xff, 0x9d, 0x3f, 128)
    elif (T < 0.2600):
        c = (0xff, 0xa5, 0x4f, 128)
    elif (T < 0.2800):
        c = (0xff, 0xad, 0x5e, 128)
    elif (T < 0.3000):
        c = (0xff, 0xb4, 0x6b, 128)
    elif (T < 0.3200):
        c = (0xff, 0xbb, 0x78, 128)
    elif (T < 0.3400):
        c = (0xff, 0xc1, 0x84, 128)
    elif (T < 0.3600):
        c = (0xff, 0xc7, 0x8f, 128)
    elif (T < 0.3800):
        c = (0xff, 0xcc, 0x99, 128)
    elif (T < 0.4000):
        c = (0xff, 0xd1, 0xa3, 128)
    elif (T < 0.4200):
        c = (0xff, 0xd5, 0xad, 128)
    elif (T < 0.4400):
        c = (0xff, 0xd9, 0xb6, 128)
    elif (T < 0.4600):
        c = (0xff, 0xdd, 0xbe, 128)
    elif (T < 0.4800):
        c = (0xff, 0xe1, 0xc6, 128)
    elif (T < 0.5000):
        c = (0xff, 0xe4, 0xce, 128)
    elif (T < 0.5200):
        c = (0xff, 0xe8, 0xd5, 128)
    elif (T < 0.5400):
        c = (0xff, 0xeb, 0xdc, 128)
    elif (T < 0.5600):
        c = (0xff, 0xee, 0xe3, 128)
    elif (T < 0.5800):
        c = (0xff, 0xf0, 0xe9, 128)
    elif (T < 0.6000):
        c = (0xff, 0xf3, 0xef, 128)
    elif (T < 0.6200):
        c = (0xff, 0xf5, 0xf5, 128)
    elif (T < 0.6400):
        c = (0xff, 0xf8, 0xfb, 128)
    elif (T < 0.6600):
        c = (0xfe, 0xf9, 0xff, 128)
    elif (T < 0.6800):
        c = (0xf9, 0xf6, 0xff, 128)
    elif (T < 0.7000):
        c = (0xf5, 0xf3, 0xff, 128)
    elif (T < 0.7200):
        c = (0xf0, 0xf1, 0xff, 128)
    elif (T < 0.7400):
        c = (0xed, 0xef, 0xff, 128)
    elif (T < 0.7600):
        c = (0xe9, 0xed, 0xff, 128)
    elif (T < 0.7800):
        c = (0xe6, 0xeb, 0xff, 128)
    elif (T < 0.8000):
        c = (0xe3, 0xe9, 0xff, 128)
    elif (T < 0.8200):
        c = (0xe0, 0xe7, 0xff, 128)
    elif (T < 0.8400):
        c = (0xdd, 0xe6, 0xff, 128)
    elif (T < 0.8600):
        c = (0xda, 0xe4, 0xff, 128)
    elif (T < 0.8800):
        c = (0xd8, 0xe3, 0xff, 128)
    elif (T < 0.9000):
        c = (0xd6, 0xe1, 0xff, 128)
    elif (T < 0.9200):
        c = (0xd3, 0xe0, 0xff, 128)
    elif (T < 0.9400):
        c = (0xd1, 0xdf, 0xff, 128)
    elif (T < 0.9600):
        c = (0xcf, 0xdd, 0xff, 128)
    elif (T < 0.9800):
        c = (0xce, 0xdc, 0xff, 128)
    else:
        c = (0xcc, 0xdb, 0xff, 128)
    return c

# Remove all visual objects
def destroy_objects():
    return None






