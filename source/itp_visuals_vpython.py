#!/usr/bin/env python
#UCAS molecular simulation, Summer 2016
#3D Visual Python Routines
#Written by Ryan Cocking & Robert Forrest

import vpython as vp
import numpy as npy
import time

import itp_params as params

scene = vp.scene
bond_line=[]
atoms0 =[]
atoms1 =[]
atoms2 =[]
atoms3 =[]
atoms4 =[]
atoms5 =[]
atoms6 =[]
atoms7 =[]
atoms=[]
fps=0
bounding_box=[]

#----------------------------#
#          DRAWING           #
#----------------------------#

#Draw particle container
def build_box(x,y,z=(0.,0.)):
    # Front face
    bounding_box = [vp.curve(pos=[(x[0],y[0],z[0]), (x[1],y[0],z[0]),
                (x[1],y[1],z[0]), (x[0],y[1],z[0]),
                (x[0],y[0],z[0])])]
    if (params.Ndim == 3):
        # Back face
        bounding_box.append(vp.curve(pos=[(x[0],y[0],z[1]), (x[1],y[0],z[1]),
                   (x[1],y[1],z[1]), (x[0],y[1],z[1]),
                   (x[0],y[0],z[1])]))
        # Connecting lines
        bounding_box.append(vp.curve(pos=[(x[0],y[0],z[0]), (x[0],y[0],z[1])]))
        bounding_box.append(vp.curve(pos=[(x[1],y[0],z[0]), (x[1],y[0],z[1])]))
        bounding_box.append(vp.curve(pos=[(x[0],y[1],z[0]), (x[0],y[1],z[1])]))
        bounding_box.append(vp.curve(pos=[(x[1],y[1],z[0]), (x[1],y[1],z[1])]))

    return bounding_box

#Initialise visuals
def build_visuals():
    global scene, fps, bounding_box

    #Create the scene

    scene = vp.scene
    scene = vp.canvas(title=' ', x=360, y=30, width=700, height=700, center = vp.vector(params.box_width*0.5,params.box_width*0.5,0.0),range=(1.1*(params.box_width/2.0)))
    scene.lights        = []
    scene.ambient       = vp.color.white
    fps                 = 0


    bounding_box = build_box(*([0,params.box_width] for i in range(params.Ndim)))
    if (params.Ndim == 2):
        # Stop the user from rotating into the 3rd dimension
        scene.userspin      = False
        scene.userzoom      = True
    return None

def build_atoms(pos):
    global atoms
    global atoms0
    global atoms1
    global atoms2
    global atoms3,atoms4, atoms5,atoms6,atoms7
    # Create P array of atoms for up to 8 beads
    for i in range(0,params.P):

      if i == 0 :
        atoms0       = npy.asarray([vp.sphere(pos=vp.vector(atom0[0],atom0[1],0.0), radius=0.525, color=vp.color.blue) for atom0 in pos[:,:,i]])
      if i == 1 :
        atoms1       = npy.asarray([vp.sphere(pos=vp.vector(atom1[0],atom1[1],0.0), radius=0.525, color=vp.color.yellow) for atom1 in pos[:,:,i]])
      if i == 2 :
        atoms2       = npy.asarray([vp.sphere(pos=vp.vector(atom2[0],atom2[1],0.0), radius=0.525, color=vp.color.red) for atom2 in pos[:,:,i]])
      if i == 3 :
        atoms3       = npy.asarray([vp.sphere(pos=vp.vector(atom3[0],atom3[1],0.0), radius=0.525, color=vp.color.green) for atom3 in pos[:,:,i]])
      if i == 4 :
        atoms4       = npy.asarray([vp.sphere(pos=vp.vector(atom4[0],atom4[1],0.0), radius=0.525, color=vp.color.cyan) for atom4 in pos[:,:,i]])
      if i == 5 :
        atoms5       = npy.asarray([vp.sphere(pos=vp.vector(atom5[0],atom5[1],0.0), radius=0.525, color=vp.color.purple) for atom5 in pos[:,:,i]])
      if i == 6 :
        atoms6       = npy.asarray([vp.sphere(pos=vp.vector(atom6[0],atom6[1],0.0), radius=0.525, color=vp.color.white) for atom6 in pos[:,:,i]])
      if i == 7 :
        atoms7       = npy.asarray([vp.sphere(pos=vp.vector(atom7[0],atom7[1],0.0), radius=0.525, color=vp.color.orange) for atom7 in pos[:,:,i]])
    atoms=[atoms0,atoms1,atoms2,atoms3,atoms4,atoms5,atoms6,atoms7]

def update_scene():
    global bounding_box
    global scene

    #Create the bounding box
    bounding_box = build_box(*((0,params.box_width) for i in range(params.Ndim)))
    #Center and range the scene
#    scene.center = [params.box_width*0.5 for i in range(params.Ndim)]
    scene.center = vp.vector(params.box_width*0.5,params.box_width*0.5,0.0)
    scene.range         = 0.55*params.box_width

# Update positions and colours of atoms
def update_visuals(q,q2=None):
    global atoms
    global atoms0
    global atoms1
    global atoms2
    global atoms3,atoms4, atoms5,atoms6,atoms7
    global scene
    global N
    global continuing
    global bonds, bond_line, fixed
    global speed_color
    global fps

    # Initialise bond lines
    bond_line           = []

    # Initial values for fps counter
    frame_times         = []
    start_t             = time.time()

    continuing = True
    while continuing:
        #Limit framerate so that sim doesnt run too fast
        vp.rate(30)

        # Get values from main thread
        pos     = q.get()
        v       = q.get()
        if (q2):
            #Handle moving individual atoms with the mouse
            picked_atom_index       = -1
            dragging_force          = npy.zeros(2)
            dragging                = False
            drag_line               = None
            already_updated         = False

#            # If there is a mouse click
#            if scene.mouse.events:
#                m1 = scene.mouse.getevent()
#                if m1.drag and m1.pick!=None:
#                    # Find atom clicked on
#                    picked_atom_index       = npy.where(atoms==m1.pick)
#                    picked_atom_index       = picked_atom_index[0][0]
#                    dragging                = True

            #While an atom is being dragged
            while dragging:
                vp.rate(30)

                collected_from_queue        = False
                # If needed, collect data from main thread
                if(pos==None):
                    collected_from_queue    = True
                    pos                     = q.get()
                    v                       = q.get()
                else:
                    q.task_done()
                    q.task_done()

                # project mouse pos onto xy plane
                new_pos     = scene.mouse.project(normal=(0,0,1))

                # Draw dragging line
                if(drag_line!= None):
                    drag_line.visible = False
                    del drag_line
                drag_line = vp.curve(pos=[vp.vector(pos[picked_atom_index]), new_pos], radius=0.05)

                # Calculate displacement between atom and mouse pos
                displacement        = npy.asarray(new_pos)[0:2] - pos[picked_atom_index]
                # Place dragging data into queue, to be collected by main thread
                q2.put(dragging)
                q2.put(picked_atom_index)
                q2.put(displacement)

                # Check if dragging has stopped
                if scene.mouse.events:
                    m1 = scene.mouse.getevent()
                    if m1.drop:
#EJH#                         main.picked_atom_index      = -1
#EJH#                         main.displacement           = npy.zeros(2)
#EJH#                         main.dragging               = False
                        dragging                    = False
                        drag_line.visible           = False
                        del drag_line

                #Update positions and colours
                already_updated     = True
                update_positions_colors(pos, v, atoms, bonds, bond_line, fixed)

                pos                 = None
                T                   = None

                if(collected_from_queue==True):
                    q.task_done()
                    q.task_done()

            # Make sure that visuals havent already been updated in dragging loop
            if(already_updated==False):
                update_positions_colors(pos, v, atoms, bonds, bond_line, fixed)
                q2.put(dragging)

                q.task_done()
                q.task_done()
        else:
            update_positions_colors(pos, v, atoms, bonds, bond_line, fixed)
            q.task_done()
            q.task_done()

        # Calculate frames per second
        end_t           = time.time()
        time_taken      = end_t - start_t
        start_t         = end_t
        frame_times.append(time_taken)
        frame_times     = frame_times[-20:]
        fps             = len(frame_times) / sum(frame_times)

def update_positions_colors(pos, v, atoms, bonds, bond_line, fixed):
    #Update positions and colours
    global atoms0
    global atoms1
    global atoms2
    global atoms3,atoms4, atoms5,atoms6,atoms7

    #Update the position of each array
    for i in range(params.N):
       for j in range(0,params.P) :
           if j == 0 :
              atoms0[i].pos    = vp.vector(pos[i][0][j],pos[i][1][j],0.0)
           if j == 1 :
              atoms1[i].pos    = vp.vector(pos[i][0][j],pos[i][1][j],0.0)
           if j == 2 :
               atoms2[i].pos    = vp.vector(pos[i][0][j],pos[i][1][j],0.0)
           if j == 3 :
               atoms3[i].pos    = vp.vector(pos[i][0][j],pos[i][1][j],0.0)
           if j == 4 :
               atoms4[i].pos    = vp.vector(pos[i][0][j],pos[i][1][j],0.0)
           if j == 5 :
              atoms5[i].pos    = vp.vector(pos[i][0][j],pos[i][1][j],0.0)
           if j == 6 :
              atoms6[i].pos    = vp.vector(pos[i][0][j],pos[i][1][j],0.0)
           if j == 7 :
              atoms7[i].pos    = vp.vector(pos[i][0][j],pos[i][1][j],0.0)

       if params.P == 1 :
           #Produce a value proportional to atom speed, used for colouring, only for classical simulation with one bead
           velocity = v[:,:,0]
           speed           = npy.sqrt(npy.dot(velocity[i],velocity[i]))
           normalised_v    = (speed*params.N)/(params.speed_colour)

           #Colour the atoms based on their speed
           atoms0[i].color  = vp.vector(normalised_v,0,1.-normalised_v) #					fix here later to add colour

    # Fixed atoms are gray
    for fix in fixed:
        atoms[fix].color = vp.color.gray(0.5)

    #Create bond lines
    for i in range(len(bonds)):
        if(bond_line[i]!=0):
            bond_line[i].visible = False

        bond_line[i] = vp.curve(pos=[vp.vector(pos[bonds[i][0]]), vp.vector(pos[bonds[i][1]])], radius=0.06)


# Remove all visual objects
def destroy_objects():
    global atoms
    global bond_line
    global bounding_box

    for bond in bond_line:
        bond.visible = False
    for beads in atoms:
        for bead in beads:
            bead.visible = False
    for line in bounding_box:
        line.visible = False

