#!/usr/bin/env python3

'''
Tetris written in Python, requires python-pygame.
Usage:
    python tetris.py [ -l <starting_level> ]

Written by Edward Higgins, 2015-02-22
'''

import pygame
import random
import copy
import getopt, sys

# Define some colors
black   = (   0,   0,   0 )
white   = ( 255, 255, 255 )
grey    = ( 127, 127, 127 )
blue    = (   0,   0, 255 )
green   = (   0, 255,   0 )
red     = ( 255,   0,   0 )
cyan    = (   0, 255, 255 )
magenta = ( 255,   0, 255 )
yellow  = ( 255, 255,   0 )

class Game():
    def __init__(self):
        self.grid = [[0 for y in range(20)] for x in range(10)]
        self.paused = True
        self.score = 0
        self.rows = 0
        self.start_level = 0
        self.level = self.start_level
        self.over = False
        self.next_grid = [[0 for y in range(2)] for x in range(4)]
        self.c_block = Block();
        self.n_block = Block();

    def display(self):
        t_font = pygame.font.SysFont('Liberation Mono', 25, True, False)
        font   = pygame.font.SysFont('Monospace', 16, True, False)

        screen.fill(black)

        next_label = t_font.render("TETRIS",True,yellow)
        screen.blit(next_label, [250, 20])

        pygame.draw.line(screen, white, [ 14,  12], [ 14, 429], 5)
        pygame.draw.line(screen, white, [ 12, 429], [231, 429], 5)
        pygame.draw.line(screen, white, [229, 429], [229,  12], 5)
        pygame.draw.line(screen, white, [229,  14], [ 14,  14], 5)
        display_grid(self.grid, self.c_block, 20, 20)

        next_label = font.render("Next:",True,white)
        screen.blit(next_label, [250, 88])
        pygame.draw.line(screen, white, [244, 80],  [244, 156], 5)
        pygame.draw.line(screen, white, [242, 156],  [338, 156], 5)
        pygame.draw.line(screen, white, [336, 156],  [336, 80], 5)
        pygame.draw.line(screen, white, [336, 82],  [244, 82], 5)
        display_grid(self.next_grid, self.n_block, 250, 108)

        next_label = font.render("Score:",True,white)
        screen.blit(next_label, [250, 170])
        pygame.draw.line(screen, white, [244,  165], [244, 241], 5)
        pygame.draw.line(screen, white, [242, 241], [338, 241], 5)
        pygame.draw.line(screen, white, [336, 241], [336,  165], 5)
        pygame.draw.line(screen, white, [336,  167], [244,  167], 5)
        next_label = font.render(str(game.score),True,white)
        screen.blit(next_label, [250, 200])

        next_label = font.render("Level:",True,white)
        screen.blit(next_label, [250, 255])
        pygame.draw.line(screen, white, [244, 250],  [244, 326], 5)
        pygame.draw.line(screen, white, [242, 326],  [338, 326], 5)
        pygame.draw.line(screen, white, [336, 326],  [336, 250], 5)
        pygame.draw.line(screen, white, [336, 252],  [244, 252], 5)
        next_label = font.render(str(game.level),True,white)
        screen.blit(next_label, [250, 285])

        next_label = font.render("Rows:",True,white)
        screen.blit(next_label, [250, 340])
        pygame.draw.line(screen, white, [244, 335],  [244, 411], 5)
        pygame.draw.line(screen, white, [242, 411],  [338, 411], 5)
        pygame.draw.line(screen, white, [336, 411],  [336, 335], 5)
        pygame.draw.line(screen, white, [336, 337],  [244, 337], 5)
        next_label = font.render(str(game.rows),True,white)
        screen.blit(next_label, [250, 370])

        if self.over:
            pygame.draw.rect(screen, black, [25, 205, 195, 75], 0)
            next_label = t_font.render("GAME OVER",True,red)
            screen.blit(next_label, [30, 210])
            next_label = font.render("Press 'r' to reset",True,red)
            screen.blit(next_label, [30, 250])

        elif self.paused:
            next_label = t_font.render("PAUSED",True,cyan)
            screen.blit(next_label, [80, 210])
            next_label = font.render("Press 'p' to toggle",True,cyan)
            screen.blit(next_label, [30, 250])

        pygame.display.flip()

class Block():
    def __init__(self):
        self.color = 0
        self.xloc  = 0
        self.yloc  = 0
        self.x     = [0 for x in range(4)]
        self.y     = [0 for x in range(4)]

    def new(self, xloc, yloc):
        self.btype = random.randrange(7)
        self.xloc = xloc
        self.yloc = yloc
        if self.btype == 0: # Line piece
            self.color = red
            self.x = [-1, 0, 1, 2]
            self.y = [0, 0, 0, 0]

        elif self.btype == 1: # Square
            self.color = green
            self.x = [0, 1, 0, 1]
            self.y = [0, 0, 1, 1]

        elif self.btype == 2: # L
            self.color = blue
            self.x = [-1, 0, 1, 1]
            self.y = [0, 0, 0, 1]

        elif self.btype == 3: # reverse L
            self.color = cyan
            self.x = [-1, 0, 1, -1]
            self.y = [0, 0, 0, 1]

        elif self.btype == 4: # S
            self.color = magenta
            self.x = [-1, 0, 0, 1]
            self.y = [0, 0, 1, 1]

        elif self.btype == 5: # reverse S
            self.color = yellow
            self.x = [0, 1, -1, 0]
            self.y = [0, 0, 1, 1]

        elif self.btype == 6: # reverse T
            self.color = white
            self.x = [0, 1, -1, 0]
            self.y = [0, 0, 0, 1]
        self.btype = self.btype + 1

    def rotate(self, grid):
        stop = False
        for i in range(4):
            if game.c_block.xloc+game.c_block.y[i] < 0 or game.c_block.xloc+game.c_block.y[i] >= 10:
                stop = True
            elif game.c_block.yloc-game.c_block.x[i] < 0 or game.c_block.yloc-game.c_block.x[i] >= 20:
                stop = True
            elif game.grid[game.c_block.xloc+game.c_block.y[i]][game.c_block.yloc-game.c_block.x[i]] != 0:
                stop = True
        if not stop:
            for i in range(4):
                tmp = game.c_block.x[i]
                game.c_block.x[i] = game.c_block.y[i]
                game.c_block.y[i] = -tmp

def display_grid(grid, block, x, y):
    nx = len(grid)
    ny = len(grid[0])
    for row in range(ny):
        for column in range(nx):
            color = black
            if grid[column][row] == 1:
                color = red
            elif grid[column][row] == 2:
                color = green
            elif grid[column][row] == 3:
                color = blue
            elif grid[column][row] == 4:
                color = cyan
            elif grid[column][row] == 5:
                color = magenta
            elif grid[column][row] == 6:
                color = yellow
            elif grid[column][row] == 7:
                color = white
            else:
                for i in range(4):
                    if (block.xloc+block.x[i] == column) and (block.yloc+block.y[i] == row):
                        color = block.color

            pygame.draw.ellipse(screen, color, [20*column+2+x, 20*row+2+y, 18, 18])

# Main program

game = Game()
game.c_block.new(4, 0)
game.n_block.new(1, 0)

opts, args = getopt.getopt(sys.argv[1:],"l:",["level="])
for opt, arg in opts:
    if opt in ("-l", "--level"):
        game.start_level = int(arg)
        game.level = game.start_level

pygame.init()

screen = pygame.display.set_mode((350, 450))

clock = pygame.time.Clock()
MOVEEVENT = pygame.USEREVENT+1
pygame.time.set_timer(MOVEEVENT, int(500.0/1.1**(game.level)))


done = False
while not done:
    # Event handling
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_p:
                game.paused = not game.paused

            elif event.key == pygame.K_r:
                game.__init__()
                game.c_block.new(4, 0)
                game.n_block.new(1, 0)
                pygame.time.set_timer(MOVEEVENT, int(500.0/1.1**(game.level)))

            elif not game.paused:
                if event.key == pygame.K_UP:
                    game.c_block.rotate(game.grid)

                elif event.key == pygame.K_LEFT:
                    stop = False
                    for i in range(4):
                        if game.c_block.xloc+game.c_block.x[i] < 1:
                            stop = True
                        elif game.grid[game.c_block.xloc+game.c_block.x[i]-1][game.c_block.yloc+game.c_block.y[i]] != 0:
                            stop = True
                    if not stop:
                        game.c_block.xloc = game.c_block.xloc - 1

                elif event.key == pygame.K_RIGHT:
                    stop = False
                    for i in range(4):
                        if game.c_block.xloc+game.c_block.x[i] > 8:
                            stop = True
                        elif game.grid[game.c_block.xloc+game.c_block.x[i]+1][game.c_block.yloc+game.c_block.y[i]] != 0:
                            stop = True
                    if not stop:
                        game.c_block.xloc = game.c_block.xloc + 1

                elif event.key == pygame.K_SPACE:
                    stop = False
                    while not stop:
                        for i in range(4):
                            if game.c_block.yloc+game.c_block.y[i] >= 19 :
                                stop = True
                            elif game.grid[game.c_block.xloc+game.c_block.x[i]][game.c_block.yloc+game.c_block.y[i]+1] != 0:
                                stop = True
                        if not stop:
                            game.c_block.yloc = game.c_block.yloc + 1
                        else:
                            for i in range(4):
                                x = game.c_block.xloc+game.c_block.x[i]
                                y = game.c_block.yloc+game.c_block.y[i]
                                game.grid[x][y] = game.c_block.btype


                    for i in range(4):
                        x = game.c_block.xloc+game.c_block.x[i]
                        y = game.c_block.yloc+game.c_block.y[i]
                        game.grid[x][y] = game.c_block.btype

                    game.c_block = copy.deepcopy(game.n_block)
                    game.c_block.xloc = 4
                    for i in range(4):
                        if game.grid[game.c_block.xloc+game.c_block.x[i]][game.c_block.yloc+game.c_block.y[i]] != 0:
                            game.paused = True
                            game.over   = True
                    game.n_block.new(1,0)

        elif event.type == MOVEEVENT:
            if not game.paused:
                stop = False
                for i in range(4):
                    if game.c_block.yloc+game.c_block.y[i] >= 19 :
                        stop = True
                    elif game.grid[game.c_block.xloc+game.c_block.x[i]][game.c_block.yloc+game.c_block.y[i]+1] != 0:
                        stop = True
                if not stop:
                    game.c_block.yloc = game.c_block.yloc + 1
                else:
                    for i in range(4):
                        x = game.c_block.xloc+game.c_block.x[i]
                        y = game.c_block.yloc+game.c_block.y[i]
                        game.grid[x][y] = game.c_block.btype

                    game.c_block = copy.deepcopy(game.n_block)
                    game.c_block.xloc = 4
                    for i in range(4):
                        if game.grid[game.c_block.xloc+game.c_block.x[i]][game.c_block.yloc+game.c_block.y[i]] != 0:
                            game.paused = True
                            game.over   = True
                    game.n_block.new(1,0)

    local_rows = 0
    row = 19
    while row >= 0:
        complete = True
        for column in range(10):
            if game.grid[column][row] == 0:
                complete = False

        if complete:
            for i in range(10):
                game.grid[i][row] = 0
            for row2 in range(row, 0, -1):
                for i in range(10):
                    game.grid[i][row2] = game.grid[i][row2-1]
            local_rows += 1

        else:
            row = row - 1

    if local_rows > 0:
        if local_rows == 1:
            game.score += 40*(game.level+1)
        if local_rows == 2:
            game.score += 100*(game.level+1)
        if local_rows == 3:
            game.score += 300*(game.level+1)
        if local_rows == 4:
            game.score += 1200*(game.level+1)

        game.rows += local_rows
        game.level = game.start_level + game.rows / 10
        pygame.time.set_timer(MOVEEVENT, int(500.0/1.1**(game.level)))

    # Update the screen
    game.display()

    # Limit to 20 frames per second
    clock.tick(20)

pygame.quit()
